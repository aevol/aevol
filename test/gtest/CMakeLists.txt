FetchContent_Declare(
    googletest
    GIT_REPOSITORY https://github.com/google/googletest.git
    GIT_TAG        v1.14.0
)
Message(STATUS "Getting external dependency (googletest)...")
FetchContent_MakeAvailable(googletest)
Message(STATUS "Getting external dependency (googletest)... Done")

# Create a ctest test and a runner for the unit test.
# Generates a list named test_runners
function(add_gtest
    test
    aevol_flavor)
  add_executable(${test} ${test}.cpp)

  set_target_properties(${test}
    PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/test"
  )

  target_link_libraries(${test} gtest_main lib${aevol_flavor})

  set(test_name "[GTEST] ${test}")
  add_test(NAME ${test_name} COMMAND ${test})

  set_tests_properties(${test_name}
      PROPERTIES LABELS "TEST;gtest;${aevol_flavor}")

  # Setup test runner
  set(test_runner run_${test})
  set(test_runners ${test_runners} ${test_runner})

  add_custom_target(${test_runner}
    COMMENT "Running test ${test}..."
    COMMAND ${CMAKE_BINARY_DIR}/test/${test}
    DEPENDS ${test}
  )

  set_target_properties(${test_runner}
    PROPERTIES
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_BINARY_DIR}/test"
  )
endfunction()

# List of all the gtests
set(AEVOL_2B_GTESTS
  DnaTest
  JumpingMTTest
  DiscreteDoubleFuzzyTest
)
set(AEVOL_4B_GTESTS
  Dna_4bTest
  FastaTest
)
set(AEVOL_EUK_2B_GTESTS
  EukIndividualTest
  EukTest_mutations
  EukTest_nornas
  EukTest_withrnas
)
set(AEVOL_GTESTS ${AEVOL_2B_GTESTS} ${AEVOL_4B_GTESTS} ${AEVOL_EUK_2B_GTESTS})

foreach (TEST IN LISTS AEVOL_2B_GTESTS)
  add_gtest(${TEST} "aevol_2b")
endforeach(TEST)
foreach (TEST IN LISTS AEVOL_4B_GTESTS)
  add_gtest(${TEST} "aevol_4b")
endforeach(TEST)
foreach (TEST IN LISTS AEVOL_EUK_2B_GTESTS)
  add_gtest(${TEST} "aevol_eukaryote_2b")
endforeach(TEST)

# Add target check (main target for tests)
add_custom_target(check DEPENDS ${test_runners})

# Add test-set targets
add_custom_target(all-with-gtests DEPENDS ${AEVOL_FLAVORS} ${AEVOL_GTESTS})
