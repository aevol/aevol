// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#ifndef AEVOL_JUMPING_MT_H_
#define AEVOL_JUMPING_MT_H_

#include <cinttypes>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <vector>

#include "SFMT-src-1.4/SFMT.h"
#include "SFMT-src-1.4/jump/SFMT-jump.h"

#include "checkpointing/mxifstream.h"
#include "checkpointing/mxofstream.h"




// MT_RAND_MAX = 2^32-1
#define MT_RAND_MAX         4294967295.0
#define MT_RAND_MAX_PLUS_1  4294967296.0

namespace aevol {

class JumpingMT
{
  public :
    // =================================================================
    //                             Constructors
    // =================================================================
    JumpingMT();
    JumpingMT(const uint32_t& simple_seed);  // Initialize with a simple uint32_t
    JumpingMT(const JumpingMT& model);       // Create a copy of an existing generator
    JumpingMT& operator=(JumpingMT rhs);

    bool operator==(const JumpingMT& rhs) const;

    // =================================================================
    //                             Destructors
    // =================================================================
    virtual ~JumpingMT();

    // =================================================================
    //                        Accessors: getters
    // =================================================================
  inline int32_t initial_seed() const {return initial_seed_;}
    // =================================================================
    //                        Accessors: setters
    // =================================================================

    // =================================================================
    //                              Operators
    // =================================================================

    // =================================================================
    //                            Public Methods
    // =================================================================
    double   random();             // Double in [0, 1[ (uniform distribution)
    int8_t   random(int8_t max);   // ~
    int16_t  random(int16_t max);  // ~
    int32_t  random(int32_t max);  // ~ > Integer in [0, max[ (uniform distribution)
    int64_t  random(int64_t max);  // ~

    // Binomial drawing of parameters (nb, prob)
    int32_t binomial_random(int32_t nb, double prob);

    // Double following a Standard Normal distribution
    double gaussian_random(double mu = 0.0, double sigma = 1.0);

    // Roulette selection
    int32_t roulette_random(double* probs, int32_t nb_elts);

    // Multinomial drawing of parameters (nb, {source[0], source[1], ... source[colors-1]})
    void multinomial_drawing (int32_t* destination, double* source, int32_t nb_drawings, int32_t colors);

    void jump();

    static int32_t nb_jumps;
    static double  jump_time;

  protected :
    sfmt_t* sfmt_;
    int32_t initial_seed_= 0;

    static double gammln(double X);

    // Friend declarations
    friend mxifstream& operator>>(mxifstream& is, JumpingMT& obj);
    friend mxofstream& operator<<(mxofstream& os, const JumpingMT& obj);
};


// =====================================================================
//                           Getters' definitions
// =====================================================================

// =====================================================================
//                           Setters' definitions
// =====================================================================

// =====================================================================
//                          Operators' definitions
// =====================================================================

// =====================================================================
//                       Inline functions' definition
// =====================================================================
/*!
  Draw a double precision real-number in [0, 1) with a uniform distribution
 */
inline double JumpingMT::random()
{
  return sfmt_genrand_real2(sfmt_);
}

/*!
  Draw an 8-bit integer in [0, max[ with a uniform distribution
 */
inline int8_t JumpingMT::random(int8_t max)
{
  return (int8_t) floor(((double)max) * sfmt_genrand_real2(sfmt_));
}

/*!
  Draw an 16-bit integer in [0, max[ with a uniform distribution
 */
inline int16_t JumpingMT::random(int16_t max)
{
  return (int16_t) floor(((double)max) * sfmt_genrand_real2(sfmt_));
}

/*!
  Draw an 32-bit integer in [0, max[ with a uniform distribution
 */
inline int32_t JumpingMT::random(int32_t max)
{
  return (int32_t) floor(((double)max) * sfmt_genrand_real2(sfmt_));
}

/*!
  Draw an 64-bit integer in [0, max[ with a uniform distribution
 */
inline int64_t JumpingMT::random(int64_t max)
{
  return (int64_t) floor(((double)max) * sfmt_genrand_real2(sfmt_));
}

} // namespace aevol

#endif // AEVOL_JUMPING_MT_H_
