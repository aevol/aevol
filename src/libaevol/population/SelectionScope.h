// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_SELECTION_SCOPE_H_
#define AEVOL_SELECTION_SCOPE_H_

#include <cstdint>
#include <iostream>
#include <string>

namespace aevol {

enum class SelectionScope : uint8_t {
  GLOBAL = 0,
  LOCAL  = 1
};

std::string to_string(const SelectionScope& v);
SelectionScope to_SelectionScope(const std::string& str);
std::istream& operator>>(std::istream&, SelectionScheme&);

inline std::string to_string(const SelectionScope& v) {
  switch (v) {
    case SelectionScope::GLOBAL:
      return "GLOBAL";
    case SelectionScope::LOCAL:
      return "LOCAL";
    default:
      return "UNKNOWN";
  }
}

inline SelectionScope to_SelectionScope(const std::string& str) {
  if (str == "GLOBAL")
    return SelectionScope::GLOBAL;
  else if (str == "LOCAL")
    return SelectionScope::LOCAL;
  else {
    std::cerr << "unexpected SelectionScope descriptor string " << str << std::endl;
    exit(1);
  }
}

inline std::istream& operator>>(std::istream& is, SelectionScope& obj) {
  auto str = std::string{};
  is >> str;
  obj = to_SelectionScope(str);

  return is;
}

}  // namespace aevol

#endif  // AEVOL_SELECTION_SCOPE_H_
