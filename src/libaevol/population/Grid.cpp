// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "Grid.h"

#include <utility>

namespace aevol {

Grid::Grid(xy_type width, xy_type height) {
  reset_grid_size(width, height);
}

GridCell& Grid::operator[](size_t pos) {
  return const_cast<GridCell&>(std::as_const(*this)[pos]);
}

const GridCell& Grid::operator[](size_t pos) const {
  return grid_1d_[pos];
}

void Grid::reset_grid_size(xy_type width, xy_type height) {
  width_ = width;
  height_ = height;

  grid_1d_.reserve(width_ * height_);

  for (auto x = decltype(width_){ 0 } ; x < width_ ; ++x) {
    for (auto y = decltype(height_){ 0 } ; y < height_ ; ++y) {
      grid_1d_.emplace_back(x, y);
    }
  }
}

}  // namespace aevol
