// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_SELECTION_SCHEME_H_
#define AEVOL_SELECTION_SCHEME_H_

#include <cstdint>
#include <iostream>
#include <string>

namespace aevol {

enum class SelectionScheme : uint8_t {
  RANK_LINEAR           = 0,
  RANK_EXPONENTIAL      = 1,
  FITNESS_PROPORTIONATE = 2,
  FITTEST               = 3
};

std::string to_string(const SelectionScheme& v);
SelectionScheme to_SelectionScheme(const std::string& str);
std::istream& operator>>(std::istream&, SelectionScheme&);

inline std::string to_string(const SelectionScheme& v) {
  switch (v) {
    case SelectionScheme::RANK_LINEAR:
      return "RANK_LINEAR";
    case SelectionScheme::RANK_EXPONENTIAL:
      return "RANK_EXPONENTIAL";
    case SelectionScheme::FITNESS_PROPORTIONATE:
      return "FITNESS_PROPORTIONATE";
    case SelectionScheme::FITTEST:
      return "FITTEST";
    default:
      return "UNKNOWN";
  }
}

inline SelectionScheme to_SelectionScheme(const std::string& str) {
  if (str == "RANK_LINEAR")
    return SelectionScheme::RANK_LINEAR;
  else if (str == "RANK_EXPONENTIAL")
    return SelectionScheme::RANK_EXPONENTIAL;
  else if (str == "FITNESS_PROPORTIONATE")
    return SelectionScheme::FITNESS_PROPORTIONATE;
  else if (str == "FITTEST")
    return SelectionScheme::FITTEST;
  else {
    std::cerr << "unexpected SelectionScheme descriptor string " << str << std::endl;
    exit(1);
  }
}

inline std::istream& operator>>(std::istream& is, SelectionScheme& obj) {
  auto str = std::string{};
  is >> str;
  obj = to_SelectionScheme(str);

  return is;
}

}  // namespace aevol

#endif  // AEVOL_SELECTION_SCHEME_H_
