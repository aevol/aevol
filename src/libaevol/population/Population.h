// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_POPULATION_H_
#define AEVOL_POPULATION_H_

#include <ranges>
#include <vector>

#include "Individual.h"
#include "indiv_idxs_map.h"

namespace aevol {

class Population {
 public:
  Population()                             = default;
  Population(const Population&)            = default;
  Population(Population&&)                 = default;
  Population& operator=(const Population&) = default;
  Population& operator=(Population&&)      = default;
  virtual ~Population();

  const auto& operator[](size_t pos) const { return *individuals_[pos]; }
  auto& operator[](size_t pos) { return *individuals_[pos]; }
  auto indiv_shared_ptr(size_t pos) { return individuals_[pos]; }
  auto size() const { return individuals_.size(); }

  auto individuals() const { return std::views::transform(individuals_, as_const_ref); }
  auto mutable_individuals() { return std::views::transform(individuals_, as_mutable_ref); }
  const auto& best_indiv() const { return *individuals_[best_indiv_idx_]; }
  auto best_indiv_idx() const { return best_indiv_idx_; }

  void update_best();
  void reset(std::vector<std::shared_ptr<Individual>> new_pop);

  void fill_with_clones(size_t nb_indivs, std::shared_ptr<Individual> indiv);
  void reset_from_idxs_map(size_t nb_indivs, indivs_idxs_map_t indivs_idxs_map);
  auto to_indivs_idxs_sorted_map() const { return aevol::to_indivs_idxs_sorted_map(individuals()); }

 protected:
  static auto as_const_ref(const std::shared_ptr<Individual>& ptr) -> const Individual& { return *ptr; }
  static auto as_mutable_ref(const std::shared_ptr<Individual>& ptr) -> Individual& { return *ptr; }

  std::vector<std::shared_ptr<Individual>> individuals_;
  indiv_id_type best_indiv_idx_ = 0;
};

}  // namespace aevol

#endif  // AEVOL_POPULATION_H_
