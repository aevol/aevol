// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "GridCell.h"

namespace aevol {

GridCell::GridCell(xy_type x, xy_type y) : x_(x), y_(y) {
  mutation_prng_  = std::make_unique<JumpingMT>();
  selection_prng_ = std::make_unique<JumpingMT>();
}

void GridCell::set_mutation_prng(const JumpingMT& mutation_prng) {
  mutation_prng_ = std::make_unique<JumpingMT>(mutation_prng);
}

void GridCell::set_selection_prng(const JumpingMT& selection_prng) {
  selection_prng_ = std::make_unique<JumpingMT>(selection_prng);
}

mxifstream& operator>>(mxifstream& is, GridCell& obj) {
  is >> *obj.mutation_prng_;
  is >> *obj.selection_prng_;
  return is;
}

mxofstream& operator<<(mxofstream& os, const GridCell& obj) {
  os << obj.mutation_prng();
  os << ' ';
  os << obj.selection_prng();
  return os;
}

}  // namespace aevol
