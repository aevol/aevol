// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "Population.h"

namespace aevol {

Population::~Population() {
}

void Population::fill_with_clones(size_t nb_indivs, std::shared_ptr<Individual> indiv) {
  individuals_.resize(nb_indivs);
  for (indiv_id_type indiv_id = 0; indiv_id < individuals_.size(); ++indiv_id) {
    individuals_[indiv_id] = indiv;
  }
}

void Population::reset_from_idxs_map(size_t nb_indivs, indivs_idxs_map_t indivs_idxs_map) {
  // Initialize DM individuals_ according to data in indivs_idxs_map
  individuals_.clear();
  individuals_.resize(nb_indivs);
  for (auto& [indiv, idxs] : indivs_idxs_map) {
    for (const auto& idx : idxs) {
      individuals_[idx] = indiv;
    }
  }
}

void Population::update_best() {
  double best_fitness = individuals_[0]->fitness();
  best_indiv_idx_ = 0;
  for (auto indiv_idx = size_t{1}; indiv_idx < individuals_.size(); ++indiv_idx) {
    if (individuals_[indiv_idx]->fitness() > best_fitness) {
      best_indiv_idx_ = indiv_idx;
      best_fitness = individuals_[indiv_idx]->fitness();
    }
  }
}

void Population::reset(std::vector<std::shared_ptr<Individual>> new_pop) {
  #pragma omp for schedule(static)
  for (indiv_id_type indiv_id = 0 ; indiv_id < individuals_.size() ; ++indiv_id) {
    individuals_[indiv_id] = new_pop[indiv_id];
    new_pop[indiv_id] = nullptr;
  }
}

}  // namespace aevol
