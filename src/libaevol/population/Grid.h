// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_GRID_H
#define AEVOL_GRID_H

#include "GridCell.h"

#include <vector>

namespace aevol {

class Grid {
 public:
  using xy_type = GridCell::xy_type;

  Grid()                       = default;
  Grid(const Grid&)            = delete;
  Grid(Grid&&)                 = default;
  Grid& operator=(const Grid&) = delete;
  Grid& operator=(Grid&&)      = default;
  virtual ~Grid()              = default;

  Grid(xy_type width, xy_type height);

  const GridCell& operator[](size_t pos) const;
  GridCell& operator[](size_t pos);

  auto width() const { return width_; };
  auto height() const { return height_; };

  void reset_grid_size(xy_type width, xy_type height);

 protected:
  xy_type width_ = 0;
  xy_type height_ = 0;
  std::vector<GridCell> grid_1d_;
};

}  // namespace aevol

#endif  // AEVOL_GRID_H
