// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_GRIDCELL_H
#define AEVOL_GRIDCELL_H

#include <cinttypes>
#include <memory>

#include "ae_types.h"
#include "checkpointing/mxifstream.h"
#include "checkpointing/mxofstream.h"
#include "JumpingMT.h"

namespace aevol {

class GridCell {
 public:
  using xy_type = grid_xy_type;

  GridCell()                           = delete;
  GridCell(const GridCell&)            = delete;
  GridCell(GridCell&&)                 = default;
  GridCell& operator=(const GridCell&) = delete;
  GridCell& operator=(GridCell&&)      = delete;

  GridCell(xy_type x, xy_type y);

  virtual ~GridCell() = default;

  auto x() const { return x_; }
  auto y() const { return y_; }
  auto& mutation_prng() const { return *mutation_prng_; }
  auto& selection_prng() const { return *selection_prng_; }

  void set_mutation_prng(const JumpingMT& mutation_prng);
  void set_selection_prng(const JumpingMT& selection_prng);

 protected:
  xy_type x_;
  xy_type y_;
  std::unique_ptr<JumpingMT> mutation_prng_;
  std::unique_ptr<JumpingMT> selection_prng_;

  // Friend declarations
  friend mxifstream& operator>>(mxifstream&, GridCell&);
};

mxofstream& operator<<(mxofstream&, const GridCell&);

}  // namespace aevol

#endif  // AEVOL_GRIDCELL_H
