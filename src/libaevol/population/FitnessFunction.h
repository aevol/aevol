// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_FITNESS_FUNCTION_H_
#define AEVOL_FITNESS_FUNCTION_H_

#include <cstdint>
#include <iostream>
#include <string>

namespace aevol {

enum class FitnessFunction : uint8_t {
  EXP = 0,
};

std::string to_string(const FitnessFunction& v);
FitnessFunction to_FitnessFunction(const std::string& str);
std::istream& operator>>(std::istream&, FitnessFunction&);

inline std::string to_string(const FitnessFunction& v) {
  switch (v) {
    case FitnessFunction::EXP:
      return "EXP";
    default:
      return "UNKNOWN";
  }
}

inline FitnessFunction to_FitnessFunction(const std::string& str) {
  if (str == "EXP")
    return FitnessFunction::EXP;
  else {
    std::cerr << "unexpected FitnessFunction descriptor string " << str << std::endl;
    exit(1);
  }
}

inline std::istream& operator>>(std::istream& is, FitnessFunction& obj) {
  auto str = std::string{};
  is >> str;
  obj = to_FitnessFunction(str);

  return is;
}

}  // namespace aevol

#endif  // AEVOL_FITNESS_FUNCTION_H_
