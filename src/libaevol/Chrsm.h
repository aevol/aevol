// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_CHRSM_H_
#define AEVOL_CHRSM_H_

#include <cstdint>
#include <initializer_list>
#include <type_traits>

#include "utility.h"

namespace aevol {

enum class Chrsm : uint8_t {
  A = 0,
  B = 1
};

static auto chrsm_ids =
#ifdef DIPLOID
    std::initializer_list<std::underlying_type_t<Chrsm>>{std::to_underlying(Chrsm::A), std::to_underlying(Chrsm::B)};
#else
    std::initializer_list<std::underlying_type_t<Chrsm>>{std::to_underlying(Chrsm::A)};
#endif

static auto chrsms =
#ifdef DIPLOID
    std::initializer_list<Chrsm>{Chrsm::A, Chrsm::B};
#else
    std::initializer_list<Chrsm>{Chrsm::A};
#endif

inline std::string to_string(const Chrsm& v) {
  switch (v) {
    case Chrsm::A:
      return "Chrsm::A";
    case Chrsm::B:
      return "Chrsm::B";
    default:
      return "UNKNOWN";
  }
}

}  // namespace aevol

#endif  // AEVOL_CHRSM_H_
