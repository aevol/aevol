// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_FUNCTIONSHAPE_H_
#define AEVOL_FUNCTIONSHAPE_H_

namespace aevol {

namespace align {

enum class FunctionShape {
  LINEAR  = 0,
  SIGMOID = 1
};

std::string to_string(const FunctionShape& v);
FunctionShape to_FunctionShape(const std::string& str);
std::istream& operator>>(std::istream&, FunctionShape&);

inline std::string to_string(const FunctionShape& v) {
  switch (v) {
    case FunctionShape::LINEAR:
      return "LINEAR";
    case FunctionShape::SIGMOID:
      return "SIGMOID";
    default:
      return "UNKNOWN";
  }
}

inline FunctionShape to_FunctionShape(const std::string& str) {
  if (str == "LINEAR")
    return FunctionShape::LINEAR;
  else if (str == "SIGMOID")
    return FunctionShape::SIGMOID;
  else {
    std::cerr << "unexpected FunctionShape descriptor string " << str << std::endl;
    exit(1);
  }
}

inline std::istream& operator>>(std::istream& is, FunctionShape& obj) {
  auto str = std::string{};
  is >> str;
  obj = to_FunctionShape(str);

  return is;
}

}  // namespace align

}  // namespace aevol

#endif  // AEVOL_FUNCTIONSHAPE_H_
