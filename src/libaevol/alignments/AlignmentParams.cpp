// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "AlignmentParams.h"

#include "utility.h"

namespace aevol {

namespace align {

auto AlignmentParams::make_from_checkpoint(std::ifstream& is, mxifstream& mxis) -> std::unique_ptr<AlignmentParams> {
  auto obj = std::make_unique<AlignmentParams>();

  obj->load_from_checkpoint(is, mxis);

  return obj;
}

auto AlignmentParams::make_from_params(int8_t match_bonus,
                                       int8_t mismatch_cost,
                                       ScoreFunction& score_function,
                                       double neighbourhood_rate,
                                       Dna::size_type max_align_size) -> std::unique_ptr<AlignmentParams> {
  auto obj = std::make_unique<AlignmentParams>();

  obj->match_bonus_        = match_bonus;
  obj->mismatch_cost_      = mismatch_cost;
  obj->score_function_     = std::make_unique<ScoreFunction>(score_function);
  obj->neighbourhood_rate_ = neighbourhood_rate;
  obj->max_align_size_     = max_align_size;

  return obj;
}

void AlignmentParams::write_to_checkpoint(std::ofstream& os, mxofstream& mxos) const {
  os << "MatchBonus: " << match_bonus_ << '\n';
  os << "MismatchCost: " << mismatch_cost_ << '\n';
  score_function_->write_to_checkpoint(os, mxos);
  os << "NeighbourhoodRate: " << neighbourhood_rate_ << '\n';
  os << "MaxAlignSize: " << max_align_size_ << '\n';
}

void AlignmentParams::load_from_checkpoint(std::ifstream& is, mxifstream& mxis) {
  get_expected_or_throw(is, "MatchBonus:", match_bonus_);
  get_expected_or_throw(is, "MismatchCost:", mismatch_cost_);
  score_function_ = ScoreFunction::make_from_checkpoint(is, mxis);
  get_expected_or_throw(is, "NeighbourhoodRate:", neighbourhood_rate_);
  get_expected_or_throw(is, "MaxAlignSize:", max_align_size_);
}

}  // namespace align

}  // namespace aevol
