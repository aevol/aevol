// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_SCOREFUNCTIONPARAMS_H_
#define AEVOL_SCOREFUNCTIONPARAMS_H_

#include <functional>
#include <memory>
#include <optional>

#include "Alignment.h"
#include "FunctionShape.h"
#include "JumpingMT.h"

#include "checkpointing/mxifstream.h"
#include "checkpointing/mxofstream.h"

namespace aevol {

namespace align {

class ScoreFunction {
 public:
  ScoreFunction() = default;
  ScoreFunction(const ScoreFunction&)            = default;
  ScoreFunction(ScoreFunction&&)                 = default;
  ScoreFunction& operator=(const ScoreFunction&) = default;
  ScoreFunction& operator=(ScoreFunction&&)      = default;
  virtual ~ScoreFunction()                       = default;

  auto operator()(JumpingMT& prng) { return score_function_(prng); }

  static auto make_from_checkpoint(std::ifstream& is, mxifstream& mxis) -> std::unique_ptr<ScoreFunction>;
  static auto make_linear_function(score_t min, score_t max) -> std::unique_ptr<ScoreFunction>;
  static auto make_sigmoid_function(score_t mean, double lambda) -> std::unique_ptr<ScoreFunction>;

  void write_to_checkpoint(std::ofstream& os, mxofstream& mxos) const;

 protected:
  FunctionShape function_shape_;  // Shape of the fn align_score -> mut_event_probability
  std::optional<std::tuple<score_t, score_t>> linear_fn_params_ = std::nullopt;  // params for a linear fn
  std::optional<std::tuple<score_t, double>> sigmoid_fn_params_ = std::nullopt;  // params for a sigmoid fn

  std::function<score_t (JumpingMT&)> score_function_ = [](JumpingMT&) { return -1; };

  void load_from_checkpoint(std::ifstream& is, mxifstream& mxis);

  static auto make_linear_lambda(score_t min, score_t max) -> std::function<score_t (JumpingMT&)>;
  static auto make_sigmoid_lambda(score_t mean, double lambda) -> std::function<score_t (JumpingMT&)>;
};

}  // namespace align

}  // namespace aevol

#endif  // AEVOL_SCOREFUNCTIONPARAMS_H_
