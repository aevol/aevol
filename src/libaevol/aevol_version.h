#include <cstdint>

namespace aevol {

  extern const char* version_string;
  extern const char* version;
  extern uint8_t const version_major;
  extern uint8_t const version_minor;

}
