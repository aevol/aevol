// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "utils.h"

namespace aevol {

/**
 * Checks the returns status of a previous posix_memalign call, and exits with an appropriate message on error
 * @param status return status of posix_memalign
 */
void check_posix_memalign(int status) {
  if(status == EINVAL) {
    std::cout << "posix_memalign: invalid alignment value (should be power of two)" << std::endl;
  }
  else if(status == ENOMEM) {
    std::cout << "posix_memalign: out of memory" << std::endl;
  }

  if(status != 0) {
    std::cout << "Aborting." << std::endl;
    exit(status);
  }
}

auto time_string() -> std::string {
  auto time = std::time(nullptr);
  auto localtime = std::localtime(&time);
  return std::format("{}-{}-{}T{:0>2}{:0>2}{:0>2}{}",
                     localtime->tm_year + 1900,
                     localtime->tm_mon + 1,
                     localtime->tm_mday,
                     localtime->tm_hour,
                     localtime->tm_min,
                     localtime->tm_sec,
                     localtime->tm_zone);
}

} // namespace aevol
