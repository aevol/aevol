// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#ifndef AEVOL_AEVOL_H_
#define AEVOL_AEVOL_H_

#include "ae_types.h"
#include "aevol_flavor.h"
#include "aevol_version.h"
#include "AeTime.h"
#include "Chrsm.h"
#include "Codon.h"
#include "Deletion.h"
#include "io/tree/DnaReplicationReport.h"
#include "Duplication.h"
#include "ExpSetup.h"
#include "Gaussian.h"
#include "Individual.h"
#include "Inversion.h"
#include "io/fasta/fasta.h"
#include "io/fasta/FastaReader.h"
#include "JumpPoly.h"
#include "JumpingMT.h"
#include "MutationReport.h"
#include "MutationParams.h"
#include "Point.h"
#include "PointMutation.h"
#include "io/tree/ReplicationReport.h"
#include "SelectionParams.h"
#include "SmallDeletion.h"
#include "SmallInsertion.h"
#include "Strand.h"
#include "Translocation.h"
#include "io/last_gener.h"
#include "io/tree/Tree.h"
#include "utility.h"
#include "utils.h"
#include "macros.h"
#include "parameters/ParamReader.h"
#include "parameters/ParameterLine.h"
#include "orchestration/orchestration.h"
#include "orchestration/CheckpointExplorer.h"
#include "orchestration/EvolutionRunner.h"
#include "orchestration/ExperimentCreator.h"
#include "orchestration/IndividualAnalyser.h"

#endif // AEVOL_AEVOL_H_
