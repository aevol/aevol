// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_ANNOTATED_CHROMOSOME_H
#define AEVOL_ANNOTATED_CHROMOSOME_H

#include <array>
#include <cinttypes>
#include <list>

#include "Dna.h"
#include "DnaFactory.h"
#include "PromoterList.h"
#include "ProteinList.h"
#include "RnaList.h"

namespace aevol {

class AnnotatedChromosome {
 public:
  AnnotatedChromosome(const AnnotatedChromosome&) = delete;
  AnnotatedChromosome(AnnotatedChromosome&&) = delete;
  AnnotatedChromosome& operator=(const AnnotatedChromosome&) = delete;
  AnnotatedChromosome& operator=(AnnotatedChromosome&&) = delete;
  ~AnnotatedChromosome();

 private:
  AnnotatedChromosome();
  AnnotatedChromosome(const std::string& seq);

 public:
  static auto make_empty() -> std::unique_ptr<AnnotatedChromosome>;
  static auto make_from_sequence(const std::string& seq) -> std::unique_ptr<AnnotatedChromosome>;
  static auto make_clone(const AnnotatedChromosome& orig) -> std::unique_ptr<AnnotatedChromosome>;
  static auto make_recombinant(const AnnotatedChromosome& chrsm1, Dna::size_type pos1,
                               const AnnotatedChromosome& chrsm2, Dna::size_type pos2)
      -> std::unique_ptr<AnnotatedChromosome>;

  auto length() const { return dna_->length(); }

  auto rnas() const { return rna_list_.rnas(); }
  auto mutable_rna_list() { return rna_list_.mutable_rnas(); }
  auto dna() const -> const Dna& { return *dna_; }
  auto mutable_dna() -> Dna& { return *dna_; }

  auto promoters() const -> const PromoterList&;
  auto mutable_promoter_list() -> PromoterList&;
  auto promoters(Strand strand) const -> const PromoterList::SingleStranded&;
  auto mutable_promoter_list(Strand strand) -> PromoterList::SingleStranded&;

  const auto& proteins() const { return proteins_; }
  auto& mutable_protein_list() { return proteins_; }

  auto compute_rna_stats() const -> std::tuple<uint32_t, float, uint32_t>;
  auto compute_gene_stats() const -> std::tuple<uint32_t, float, uint32_t>;
  auto compute_non_coding() const -> dna_size_type;

  void reset_promoter_lists();

  void look_for_new_promoters_around(Dna::size_type pos);

  Dna* dna_ = nullptr;
  PromoterList promoter_list_;
  RnaList rna_list_;
  ProteinList proteins_;
};

} // namespace aevol

#endif // AEVOL_ANNOTATED_CHROMOSOME_H
