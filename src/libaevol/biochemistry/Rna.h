// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#ifndef AEVOL_RNA_H_
#define AEVOL_RNA_H_

#include <cstdint>

#include <list>
#include <vector>

#include "macros.h"
#include "Promoter.h"
#include "Strand.h"
#include "utils.h"

namespace aevol {

class Protein;

class AffinityFactor {
 public:
  AffinityFactor(Protein* prot, double efactor, double ofactor) {
    protein = prot;
    enhancer_factor = efactor;
    operator_factor = ofactor;
  }

  double concentration();

  Protein* protein;
  double enhancer_factor;
  double operator_factor;
};


class Rna {
 public:
 /*
  RNA Constructor
  */
  Rna() = delete;
  Rna(Promoter* promoter, int32_t length, int32_t dna_size);
  Rna(const Rna&) = delete;
  ~Rna() = default;

  auto first_transcribed_pos() const { return first_transcribed_pos_; }
  auto length() const { return length_; }

  /*
    RNA variables
  */
  int32_t begin = -1; /**< position of the promoter */
  int32_t first_transcribed_pos_ = -1; /**< position of the first transcribed position (= base right after promoter) */
  int32_t end = -1; /**< position of the last base of the terminator */
  Strand strand_;
  double e = -1.0;
  std::list<int32_t> rbs_positions_;
  std::list<Protein*> protein_list_;
 protected:
  int32_t length_           = -1; /**< length of the RNA (including terminator; not including promoter) */
 public:
  bool is_coding_ = false;

  bool is_init_ = false;
  bool to_compute_start_pos_ = true;

  Promoter* prom = nullptr;
};

}

#endif  // AEVOL_RNA_H_
