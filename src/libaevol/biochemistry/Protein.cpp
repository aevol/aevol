// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#include "Protein.h"
#include "Rna.h"

namespace aevol {

Protein::Protein(Dna::size_type position_first_aa,
                 Dna::size_type position_last_base_stop_codon,
                 size_t nb_aa,
                 Strand strand,
                 double e,
                 Rna* rna) {
  position_first_aa_             = position_first_aa;
  position_last_base_stop_codon_ = position_last_base_stop_codon;
  nb_aa_                         = nb_aa;
  strand_                        = strand;
  e_                             = e;

  rna_list_.push_back(rna);
}

Protein::~Protein() {
  rna_list_.clear();
}

bool Protein::operator<(const Protein& other){
    return (h_ <  other.h_)
           || (h_ == other.h_ && m_ < other.m_)
           || (h_ == other.h_ && m_ == other.m_ && w_ < other.w_)
           || (h_ == other.h_ && m_ == other.m_ && w_ == other.w_ && e_ < other.e_);
}

}  // namespace aevol
