// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_PROTEINLIST_H_
#define AEVOL_PROTEINLIST_H_

#include <list>

#include "Protein.h"

namespace aevol {

class ProteinList {
 public:
  ProteinList()                              = default;
  ProteinList(const ProteinList&)            = delete;
  ProteinList(ProteinList&&)                 = delete;
  ProteinList& operator=(const ProteinList&) = delete;
  ProteinList& operator=(ProteinList&&)      = delete;
  virtual ~ProteinList();

  void add(Protein* prot);
  void clear();

  auto begin() const { return proteins_.begin(); }
  auto end() const { return proteins_.end(); }
  auto size() const { return proteins_.size(); }

 protected:
  // Main protein list - contains all the proteins regardless of their origin
  std::list<Protein*> proteins_;
};

} // namespace aevol

#endif  // AEVOL_PROTEINLIST_H_
