// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_MUTATIONMETRICS_H_
#define AEVOL_MUTATIONMETRICS_H_

#include <cstdint>

namespace aevol {

struct MutationMetrics {
  int32_t nb_switch          = 0;
  int32_t nb_indels          = 0;
  int32_t nb_local_mutations = 0;

  int32_t nb_large_dupl     = 0;
  int32_t nb_large_del      = 0;
  int32_t nb_large_trans    = 0;
  int32_t nb_large_inv      = 0;
  int32_t nb_rearrangements = 0;
};

}  // namespace aevol

#endif  // AEVOL_MUTATIONMETRICS_H_
