// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#ifndef AEVOL_MUTATION_PARAMS_H_
#define AEVOL_MUTATION_PARAMS_H_

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "checkpointing/mxifstream.h"
#include "checkpointing/mxofstream.h"

namespace aevol {

// =================================================================
//                          Class declarations
// =================================================================






class MutationParams
{
  public :
    // =================================================================
    //                             Constructors
    // =================================================================
    MutationParams();
    MutationParams(const MutationParams & model);


    // =================================================================
    //                             Destructors
    // =================================================================
    virtual ~MutationParams();

    static std::unique_ptr<MutationParams> make_from_checkpoint(std::ifstream& is, mxifstream& mxis);

    // =================================================================
    //                        Accessors: getters
    // =================================================================

    // --------------------------------------------------------- Mutation rates
    inline double   point_mutation_rate() const;
    inline double   small_insertion_rate() const;
    inline double   small_deletion_rate() const;
    inline int16_t  max_indel_size() const;

    // ------------------------------ Rearrangement rates (without alignements)
    inline double duplication_rate() const;
    inline double deletion_rate() const;
    inline double translocation_rate() const;
    inline double inversion_rate() const;

    // =================================================================
    //                        Accessors: setters
    // =================================================================

    // --------------------------------------------------------- Mutation rates
    inline void set_point_mutation_rate(double point_mutation_rate);
    inline void set_small_insertion_rate(double small_insertion_rate);
    inline void set_small_deletion_rate(double small_deletion_rate);
    inline void set_max_indel_size(int16_t max_indel_size);

    // ------------------------------ Rearrangement rates (without alignements)
    inline void set_duplication_rate(double duplication_rate);
    inline void set_deletion_rate(double deletion_rate);
    inline void set_translocation_rate(double translocation_rate);
    inline void set_inversion_rate(double inversion_rate);

    // =================================================================
    //                              Operators
    // =================================================================

    // =================================================================
    //                            Public Methods
    // =================================================================
    void write_to_checkpoint(std::ofstream& os, mxofstream& mxos) const;
    void load_from_checkpoint(std::ifstream& is, mxifstream& mxis);

    // =================================================================
    //                           Public Attributes
    // =================================================================





  protected :

    // =================================================================
    //                         Forbidden Constructors
    // =================================================================
    /*MutationParams()
    {
      printf("%s:%d: error: call to forbidden constructor.\n", __FILE__, __LINE__);
      exit(EXIT_FAILURE);
    };
    MutationParams(const MutationParams &model)
    {
      printf("%s:%d: error: call to forbidden constructor.\n", __FILE__, __LINE__);
      exit(EXIT_FAILURE);
    };*/


    // =================================================================
    //                           Protected Methods
    // =================================================================

    // =================================================================
    //                          Protected Attributes
    // =================================================================

    // --------------------------------------------------------- Mutation rates
    double  point_mutation_rate_;
    double  small_insertion_rate_;
    double  small_deletion_rate_;
    int16_t max_indel_size_;

    // ------------------------------ Rearrangement rates (without alignements)
    double duplication_rate_;
    double deletion_rate_;
    double translocation_rate_;
    double inversion_rate_;
};

std::ostream& operator<<(std::ostream&, const MutationParams&);


// =====================================================================
//                           Getters' definitions
// =====================================================================

// ------------------------------------------------------------- Mutation rates
inline double MutationParams::point_mutation_rate() const
{
  return point_mutation_rate_;
}

inline double MutationParams::small_insertion_rate() const
{
  return small_insertion_rate_;
}

inline double MutationParams::small_deletion_rate() const
{
  return small_deletion_rate_;
}

inline int16_t MutationParams::max_indel_size() const
{
  return max_indel_size_;
}

// ---------------------------------- Rearrangement rates (without alignements)
inline double MutationParams::duplication_rate() const
{
  return duplication_rate_;
}

inline double MutationParams::deletion_rate() const
{
  return deletion_rate_;
}

inline double MutationParams::translocation_rate() const
{
  return translocation_rate_;
}

inline double MutationParams::inversion_rate() const
{
  return inversion_rate_;
}

// =====================================================================
//                           Setters' definitions
// =====================================================================

// ------------------------------------------------------------- Mutation rates
inline void MutationParams::set_point_mutation_rate(double point_mutation_rate)
{
  point_mutation_rate_ = point_mutation_rate;
}

inline void MutationParams::set_small_insertion_rate(double small_insertion_rate)
{
  small_insertion_rate_ = small_insertion_rate;
}

inline void MutationParams::set_small_deletion_rate(double small_deletion_rate)
{
  small_deletion_rate_ = small_deletion_rate;
}

inline void MutationParams::set_max_indel_size(int16_t max_indel_size)
{
  max_indel_size_ = max_indel_size;
}

// ---------------------------------- Rearrangement rates (without alignements)
inline void MutationParams::set_duplication_rate(double duplication_rate)
{
  duplication_rate_ = duplication_rate;
}

inline void MutationParams::set_deletion_rate(double deletion_rate)
{
  deletion_rate_ = deletion_rate;
}

inline void MutationParams::set_translocation_rate(double translocation_rate)
{
  translocation_rate_ = translocation_rate;
}

inline void MutationParams::set_inversion_rate(double inversion_rate)
{
  inversion_rate_ = inversion_rate;
}

// =====================================================================
//                          Operators' definitions
// =====================================================================

// =====================================================================
//                       Inline functions' definition
// =====================================================================

} // namespace aevol

#endif // AEVOL_MUTATION_PARAMS_H_
