// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_PROKARYOTE_INDIVIDUAL_MUTATOR_H_
#define AEVOL_PROKARYOTE_INDIVIDUAL_MUTATOR_H_

#include "IndividualMutator.h"

namespace aevol {

class ProkaryoteIndividualMutator: public IndividualMutator {
 public:
  ProkaryoteIndividualMutator()                                              = default;
  ProkaryoteIndividualMutator(const ProkaryoteIndividualMutator&)            = delete;
  ProkaryoteIndividualMutator(ProkaryoteIndividualMutator&&)                 = delete;
  ProkaryoteIndividualMutator& operator=(const ProkaryoteIndividualMutator&) = delete;
  ProkaryoteIndividualMutator& operator=(ProkaryoteIndividualMutator&&)      = delete;
  virtual ~ProkaryoteIndividualMutator()                                     = default;

  auto has_mutations() const -> bool override;

  void generate_mutations(JumpingMT& prng,
                          const Individual& indiv,
                          const MutationParams& mut_params,
                          Dna::size_type min_genome_length,
                          Dna::size_type max_genome_length) override;

  auto apply_mutations(Individual& indiv) -> std::list<std::unique_ptr<MutationReport>> override;
  void apply_mutations(Individual& indiv, indiv_id_type indiv_id, PhylogeneticTreeHandler* tree_handler) override;

 protected:
  std::unique_ptr<DnaMutator> dna_mutator_;
};

}  // namespace aevol

#endif  // AEVOL_PROKARYOTE_INDIVIDUAL_MUTATOR_H_
