// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_DNAMUTATOR_H
#define AEVOL_DNAMUTATOR_H

#include <list>
#include <memory>

#include "biochemistry/mutations/MutationEvent.h"
#include "Dna.h"
#include "JumpingMT.h"

namespace aevol {
class DnaMutator {
 public:
  DnaMutator(Dna::size_type initial_genome_size,
             int16_t max_indel_size,
             int32_t min_genome_size,
             int32_t max_genome_size);

  ~DnaMutator() = default;

  void compute_nb_mutations_per_type(JumpingMT& prng,
                                     double duplication_rate,
                                     double deletion_rate,
                                     double translocation_rate,
                                     double inversion_rate,
                                     double point_mutation_rate,
                                     double small_insertion_rate,
                                     double small_deletion_rate);
  void generate_all_mutations(JumpingMT& prng);

  // Accessors
  auto mutations_to_generate() { return ((nb_rears_to_generate_ + nb_local_muts_to_generate_) > 0); }
  auto has_mutations() const { return (not mutation_list_.empty()); }
  const auto& mutation_list() const { return mutation_list_; }
  auto final_genome_size() const { return genome_size_after_mutations_.back(); }

  // Utility static functions
  static auto generate_large_duplication_event(
      JumpingMT& prng,
      Dna::size_type& genome_size,
      Dna::size_type max_genome_size) -> std::unique_ptr<MutationEvent>;
  static auto generate_large_deletion_event(
      JumpingMT& prng,
      Dna::size_type& genome_size,
      Dna::size_type min_genome_size) -> std::unique_ptr<MutationEvent>;
  static auto generate_large_translocation_event(
      JumpingMT& prng,
      Dna::size_type genome_size) -> std::unique_ptr<MutationEvent>;
  static auto generate_large_inversion_event(
      JumpingMT& prng,
      Dna::size_type genome_size) -> std::unique_ptr<MutationEvent>;

  static auto generate_switch_event(
      JumpingMT& prng,
      Dna::size_type genome_size) -> std::unique_ptr<MutationEvent>;
  static auto generate_small_insertion_event(
      JumpingMT& prng,
      Dna::size_type& genome_size,
      Dna::size_type max_genome_size,
      Dna::size_type max_indel_size) -> std::unique_ptr<MutationEvent>;
  static auto generate_small_deletion_event(
      JumpingMT& prng,
      Dna::size_type& genome_size,
      Dna::size_type min_genome_size,
      Dna::size_type max_indel_size) -> std::unique_ptr<MutationEvent>;

 protected:
  void generate_next_mutation(JumpingMT& prng);

  /// List of mutations undergone or yet to be undergone
  std::list<std::unique_ptr<MutationEvent>> mutation_list_;
  /// Initial size of the genome
  Dna::size_type initial_genome_size_;
  /// List of successive genome sizes after each of the mutational events stored in mutation_list_
  std::list<Dna::size_type> genome_size_after_mutations_;

  // Configuration
  int16_t max_indel_size_;
  Dna::size_type min_genome_size_;
  Dna::size_type max_genome_size_;

  //--------------------------- Mutation counters
  int32_t nb_switch_to_generate_;
  int32_t nb_small_insertions_to_generate_;
  int32_t nb_small_deletions_to_generate_;
  int32_t nb_local_muts_to_generate_;

  int32_t nb_large_duplications_to_generate_;
  int32_t nb_large_deletions_to_generate_;
  int32_t nb_large_translocations_to_generate_;
  int32_t nb_large_inversions_to_generate_;
  int32_t nb_rears_to_generate_;
};

}  // namespace aevol

#endif  // AEVOL_DNAMUTATOR_H
