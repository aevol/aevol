// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "ProkaryoteIndividualMutator.h"

#include "Individual.h"
#include "io/tree/PhylogeneticTreeHandler.h"

namespace aevol {

auto ProkaryoteIndividualMutator::has_mutations() const -> bool {
  return (dna_mutator_->has_mutations());
}

void ProkaryoteIndividualMutator::generate_mutations(JumpingMT& prng,
                                           const Individual& indiv,
                                           const MutationParams& mut_params,
                                           Dna::size_type min_genome_length,
                                           Dna::size_type max_genome_length) {
  dna_mutator_ = std::make_unique<DnaMutator>(indiv.dna().length(),
                                              mut_params.max_indel_size(),
                                              min_genome_length,
                                              max_genome_length);

  dna_mutator_->compute_nb_mutations_per_type(prng,
                                              mut_params.duplication_rate(),
                                              mut_params.deletion_rate(),
                                              mut_params.translocation_rate(),
                                              mut_params.inversion_rate(),
                                              mut_params.point_mutation_rate(),
                                              mut_params.small_insertion_rate(),
                                              mut_params.small_deletion_rate());

  if (dna_mutator_->mutations_to_generate()) {
    dna_mutator_->generate_all_mutations(prng);
  }
}

auto ProkaryoteIndividualMutator::apply_mutations(Individual& indiv) -> std::list<std::unique_ptr<MutationReport>> {
  return indiv.mutable_dna().apply_mutations(dna_mutator_->mutation_list());
}

void ProkaryoteIndividualMutator::apply_mutations(
    Individual& indiv, indiv_id_type indiv_id, PhylogeneticTreeHandler* tree_handler) {
  auto undergone_mutations = apply_mutations(indiv);

  for (auto& mut : undergone_mutations) {
    if (tree_handler) {
      tree_handler->report_mutation(AeTime::time(), indiv_id, std::move(mut));
    }
  }
}

}  // namespace aevol
