// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_TRANSLATION_INITIATION_SEQUENCE_H_
#define AEVOL_TRANSLATION_INITIATION_SEQUENCE_H_

#include "Dna.h"

namespace aevol {

#ifdef BASE_2
#if defined(__INTEL_COMPILER) || defined(__INTEL_LLVM_COMPILER)
__declspec(align(64)) constexpr const char* SHINE_DAL_SEQ_LEAD = "0110111111000";
__declspec(align(64)) constexpr const char* SHINE_DAL_SEQ_LAG  = "1001001111111";
#else // defined(__INTEL_COMPILER) || defined(__INTEL_LLVM_COMPILER)
constexpr const char* SHINE_DAL_SEQ_LEAD = "0110111111000";
constexpr const char* SHINE_DAL_SEQ_LAG  = "1001001111111";
#endif // defined(__INTEL_COMPILER) || defined(__INTEL_LLVM_COMPILER)
#endif // BASE_2

class TranslationInitiationSequence {
 public:
  TranslationInitiationSequence()                                                = delete;
  TranslationInitiationSequence(const TranslationInitiationSequence&)            = delete;
  TranslationInitiationSequence(TranslationInitiationSequence&&)                 = delete;
  TranslationInitiationSequence& operator=(const TranslationInitiationSequence&) = delete;
  TranslationInitiationSequence& operator=(TranslationInitiationSequence&&)      = delete;
  virtual ~TranslationInitiationSequence()                                       = delete;

  static bool is_translation_initiation_sequence(const Dna& dna, Dna::size_type pos, Strand strand);

 protected:
  static bool is_shine_dalgarno(const Dna& dna, Dna::size_type pos, Strand strand);
  static bool is_start_codon(const Dna& dna, Dna::size_type pos, Strand strand);
};

}  // namespace aevol

#endif  // AEVOL_TRANSLATION_INITIATION_SEQUENCE_H_
