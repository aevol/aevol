// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "Rna.h"
#include "Protein.h"
#include "fuzzytypes.h"

namespace aevol {

Rna::Rna(Promoter* promoter, int32_t length, int32_t dna_size) {
  begin                  = promoter->pos();
  first_transcribed_pos_ = (promoter->strand() == Strand::LEADING)
                               ? mod(promoter->pos() + PROM_SIZE, dna_size)
                               : mod(promoter->pos() - (PROM_SIZE), dna_size);
  end     = (promoter->strand() == Strand::LEADING) ? mod(promoter->pos() + PROM_SIZE + length - 1, dna_size)
                                                    : mod(promoter->pos() - (PROM_SIZE + length - 1), dna_size);
  strand_ = promoter->strand();
  e       = promoter->basal_level();
  length_ = length;

  is_coding_            = false;
  is_init_              = true;
  to_compute_start_pos_ = true;
  prom                  = promoter;
  prom->reset_rna(this);
}

}  // namespace aevol
