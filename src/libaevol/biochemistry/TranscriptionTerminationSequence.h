// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_TRANSCRIPTION_TERMINATION_SEQUENCE_H_
#define AEVOL_TRANSCRIPTION_TERMINATION_SEQUENCE_H_

#include "Dna.h"

namespace aevol {

class TranscriptionTerminationSequence {
 public:
  TranscriptionTerminationSequence()                                                   = delete;
  TranscriptionTerminationSequence(const TranscriptionTerminationSequence&)            = delete;
  TranscriptionTerminationSequence(TranscriptionTerminationSequence&&)                 = delete;
  TranscriptionTerminationSequence& operator=(const TranscriptionTerminationSequence&) = delete;
  TranscriptionTerminationSequence& operator=(TranscriptionTerminationSequence&&)      = delete;
  virtual ~TranscriptionTerminationSequence()                                          = delete;

  static bool is_terminator(const Dna& dna, Dna::size_type pos, Strand strand);

 protected:
  static bool is_stem_loop(const Dna& dna, Dna::size_type pos, Strand strand);
};

}  // namespace aevol

#endif  // AEVOL_TRANSCRIPTION_TERMINATION_SEQUENCE_H_
