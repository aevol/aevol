// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_RNALIST_H_
#define AEVOL_RNALIST_H_

#include <cinttypes>
#include <memory>
#include <ranges>

#include "Promoter.h"
#include "Rna.h"
#include "Strand.h"

namespace aevol {

class RnaList {
 public:
  RnaList()                          = default;
  RnaList(const RnaList&)            = delete;
  RnaList(RnaList&&)                 = delete;
  RnaList& operator=(const RnaList&) = delete;
  RnaList& operator=(RnaList&&)      = delete;
  virtual ~RnaList();

  void rna_add(Promoter* prom, int32_t length, int32_t last_transcribed_base);
  void clear();

  auto rnas() const { return std::views::transform(rnas_, as_const_ref); }
  auto mutable_rnas() { return std::views::transform(rnas_, as_mutable_ref); }
  auto size() const { return rnas_.size(); }

 protected:
  std::list<std::unique_ptr<Rna>> rnas_;

  static auto as_const_ref(const std::unique_ptr<Rna>& ptr) -> const Rna& { return *ptr; }
  static auto as_mutable_ref(const std::unique_ptr<Rna>& ptr) -> Rna& { return *ptr; }
};

} // namespace aevol

#endif  // AEVOL_RNALIST_H_
