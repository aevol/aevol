// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************




// =================================================================
//                              Includes
// =================================================================
#include "Codon.h"

#include <cmath>

#include "utils.h"

namespace aevol {



// ############################################################################
//
//                                Class Codon
//
// ############################################################################

// =================================================================
//                    Definition of static attributes
// =================================================================

// =================================================================
//                             Constructors
// =================================================================
Codon::Codon() {
  value_ = -1;
}

Codon::Codon(const Codon &model) {
  value_ = model.value_;
  #ifdef BASE_4
  amino_acid_ = model.amino_acid_;
  #endif
}

Codon::Codon(int8_t value) {
  value_ = value;
  #ifdef BASE_4
  amino_acid_ =  to_aminoacid();
  #endif
}

#ifdef BASE_4
Codon::Codon(char b1, char b2, char b3) {
  amino_acid_ = to_aminoacid();
  value_ = bases_to_codon_value(b1, b2, b3);
}
#endif

Codon::~Codon() = default;

} // namespace aevol
