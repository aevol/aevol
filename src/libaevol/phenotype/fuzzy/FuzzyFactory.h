// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons, Jonathan Rouzaud-Cornabas
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_FUZZYFACTORY_H
#define AEVOL_FUZZYFACTORY_H

#include "AbstractFuzzy.h"
#include "FuzzyFlavor.h"

#include <vector>

namespace aevol {

class FuzzyFactory {
 public:
  FuzzyFactory()                                 = delete;
  FuzzyFactory(const FuzzyFactory&)            = delete;
  FuzzyFactory(FuzzyFactory&&)                 = delete;
  FuzzyFactory& operator=(const FuzzyFactory&) = delete;
  FuzzyFactory& operator=(FuzzyFactory&&)      = delete;
  ~FuzzyFactory();

  FuzzyFactory(const FuzzyFlavor& flavor, size_t sampling, size_t pop_size = 0);

  static auto make_fuzzy(FuzzyFlavor flavor, size_t sampling) -> std::unique_ptr<AbstractFuzzy>;

  void init(size_t pop_size);
  auto get_fuzzy() const;

  static auto flavor()  { return flavor_; };
  static auto sampling() { return sampling_; };

 private:
  auto createFuzzy() const -> std::unique_ptr<AbstractFuzzy>;

  static std::vector<AbstractFuzzy*> available_objects_;
  static std::vector<std::unique_ptr<AbstractFuzzy>> data_;

  static FuzzyFlavor flavor_;
  static size_t sampling_;
};

inline auto FuzzyFactory::get_fuzzy() const {
  AbstractFuzzy* raw = nullptr;

  #pragma omp critical(fuzzy_pool)
  {
    if (not available_objects_.empty()) {
      raw = available_objects_.back();
      available_objects_.pop_back();
    } else {
      data_.push_back(createFuzzy());
      raw = data_.back().get();
    }
  }

  auto custom_deleter = [](AbstractFuzzy* obj) {
    obj->clear();
    #pragma omp critical(fuzzy_pool)
    {
      available_objects_.push_back(obj);
    }
  };

  return std::unique_ptr<AbstractFuzzy, decltype(custom_deleter)>(raw, custom_deleter);
}

using fuzzy_unique_ptr = decltype(std::declval<FuzzyFactory>().FuzzyFactory::get_fuzzy());

}  // namespace aevol

#endif  // AEVOL_FUZZYFACTORY_H
