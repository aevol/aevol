// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons, Jonathan Rouzaud-Cornabas
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_FUZZYFLAVOR_H_
#define AEVOL_FUZZYFLAVOR_H_

#include <cstdint>
#include <iostream>
#include <string>

namespace aevol {

enum class FuzzyFlavor : uint8_t {
  UNSET = 0,
  VECTOR = 1,
  DISCRETE_DOUBLE_TABLE = 3
};

std::string to_string(const FuzzyFlavor& ff);
FuzzyFlavor to_FuzzyFlavor(const std::string& str);
std::istream& operator>>(std::istream&, FuzzyFlavor&);

inline std::string to_string(const FuzzyFlavor& ff) {
  switch (ff) {
    case FuzzyFlavor::VECTOR : return "VECTOR";
    case FuzzyFlavor::DISCRETE_DOUBLE_TABLE : return "DISCRETE_DOUBLE_TABLE";
    default: return "UNKNOWN";
  }
}

inline FuzzyFlavor to_FuzzyFlavor(const std::string& str) {
  if (str == "VECTOR") return FuzzyFlavor::VECTOR;
  else if (str == "DISCRETE_DOUBLE_TABLE") return FuzzyFlavor::DISCRETE_DOUBLE_TABLE;
  else {
    std::cerr << "unexpected FuzzyFlavor descriptor string " << str << std::endl;
    exit(1);
  }
}

inline std::istream& operator>>(std::istream& is, FuzzyFlavor& obj) {
  auto str = std::string{};
  is >> str;
  obj = to_FuzzyFlavor(str);

  return is;
}

}  // namespace aevol

#endif  // AEVOL_FUZZYFLAVOR_H_
