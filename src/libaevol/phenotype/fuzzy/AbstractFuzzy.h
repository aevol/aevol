// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons, Jonathan Rouzaud-Cornabas
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#ifndef AEVOL_ABSTRACT_FUZZY_H
#define AEVOL_ABSTRACT_FUZZY_H


#include <list>

#include "macros.h"
#include "Point.h"
#include "fuzzytypes.h"
#include "checkpointing/mxifstream.h"
#include "checkpointing/mxofstream.h"

namespace aevol {

class AbstractFuzzy {
 public:
  // ==========================================================================
  //                                Ctors-Dtor
  // ==========================================================================
  virtual ~AbstractFuzzy() {};

  // ==========================================================================
  //                              Public Methods
  // ==========================================================================
  virtual void reset() = 0;
  virtual void simplify() = 0;
  virtual void add_triangle(fuzzy_x_t mean,
                            fuzzy_x_t half_width,
                            fuzzy_y_t height,
                            bool verbose = false)  = 0;

  virtual void add(const AbstractFuzzy& f, bool verbose = false)  = 0;
  virtual void sub(const AbstractFuzzy& f, bool verbose = false) = 0;
  virtual void copy(const AbstractFuzzy& f, bool verbose = false) = 0;

  virtual void add_point(fuzzy_x_t x, fuzzy_y_t y) = 0;

  /// `clipping_direction` is only used for `clip` function's keyword.
  enum clipping_direction: bool {min, max};
  virtual void clip(clipping_direction direction, fuzzy_y_t bound) = 0;

  virtual bool is_identical_to(const AbstractFuzzy& fs, double tolerance) const = 0;

  virtual void print() const = 0;

  virtual void clear() = 0;

  virtual size_t nb_points() const = 0;
  virtual Point point(size_t i) const = 0;
  virtual fuzzy_area_t get_geometric_area(bool verbose = false) const = 0;
  virtual fuzzy_area_t get_geometric_area(fuzzy_x_t x_start, fuzzy_x_t x_stop) const = 0;

 protected:
  virtual mxifstream& do_extraction(mxifstream& is) = 0;
  virtual mxofstream& do_insertion(mxofstream& os) const = 0;

  // Friend declarations
  friend mxifstream& operator>>(mxifstream& is, AbstractFuzzy& obj) {
    return obj.do_extraction(is);
  }
  friend mxofstream& operator<<(mxofstream& os, const AbstractFuzzy& obj) {
    return obj.do_insertion(os);
  }
};

}  // namespace aevol

#endif //AEVOL_ABSTRACT_FUZZY_H
