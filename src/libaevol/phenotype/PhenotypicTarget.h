// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_PHENOTYPIC_TARGET_H_
#define AEVOL_PHENOTYPIC_TARGET_H_

#include <memory>

#include "phenotype/fuzzy/AbstractFuzzy.h"
#include "phenotype/fuzzy/FuzzyFactory.h"
#include "phenotype/Gaussian.h"

namespace aevol {

class PhenotypicTarget {
 public:
  PhenotypicTarget()                                   = default;
  PhenotypicTarget(const PhenotypicTarget&)            = delete;
  PhenotypicTarget(PhenotypicTarget&&)                 = delete;
  PhenotypicTarget& operator=(const PhenotypicTarget&) = delete;
  PhenotypicTarget& operator=(PhenotypicTarget&&)      = delete;
  virtual ~PhenotypicTarget()                          = default;

  static auto make_from_checkpoint(std::ifstream& is, mxifstream& mxis, FuzzyFlavor flavor, size_t sampling)
      -> std::unique_ptr<PhenotypicTarget>;
  static auto make_from_gaussians(const std::list<Gaussian>& gaussians, FuzzyFlavor flavor, size_t sampling)
      -> std::unique_ptr<PhenotypicTarget>;

  void write_to_checkpoint(std::ofstream& os, mxofstream& mxos) const;
  void load_from_checkpoint(std::ifstream& is, mxifstream& mxis, FuzzyFlavor flavor, size_t sampling);

  const auto& fuzzy() const { return *fuzzy_; }
  auto& fuzzy() { return *fuzzy_; }

  auto nb_points() const { return fuzzy_->nb_points(); };
  auto point(size_t i) const { return fuzzy_->point(i); };

  static auto build(std::ranges::input_range auto&& gaussians, FuzzyFlavor flavor, size_t sampling);

 protected:
  /// Current Phenotypic Target (FuzzySet)
  std::unique_ptr<AbstractFuzzy> fuzzy_;

  /// Phenotypic target's constitutive Gaussians in their initial state
  std::list<std::unique_ptr<Gaussian>> initial_gaussians_;
};

auto PhenotypicTarget::build(std::ranges::input_range auto&& gaussians, FuzzyFlavor flavor, size_t sampling) {
  auto built = FuzzyFactory::make_fuzzy(flavor, sampling);

  for (auto i = decltype(sampling){0}; i <= sampling; ++i) {
    auto x = fuzzy_x_t{X_MIN + static_cast<double>(i) * (X_MAX - X_MIN) / static_cast<double>(sampling)};
    auto y = fuzzy_y_t{0};

    for (const auto& gaussian: gaussians) {
      y += gaussian->compute_y(x);
    }

    //    fuzzy_->add_point(x, y);
    built->add_point(x, y);
  }

  built->clip(AbstractFuzzy::min, Y_MIN);
  built->clip(AbstractFuzzy::max, Y_MAX);

  return built;
}

}  // namespace aevol

#endif  // AEVOL_PHENOTYPIC_TARGET_H_
