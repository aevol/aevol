// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************


#ifndef AEVOL_AE_TYPES_H_
#define AEVOL_AE_TYPES_H_

#include <array>
#include <cstdint>

namespace aevol {

using dna_size_type = int32_t;
using grid_xy_type  = uint16_t;
using indiv_id_type = uint32_t;
using indiv_id_difference_type = int32_t;
using time_type     = int64_t;

#ifndef SEX
using reproducers_info_t = indiv_id_type;
#else
using reproducers_info_t = std::array<indiv_id_type, 2>;
#endif

} // namespace aevol

#endif // AEVOL_AE_TYPES_H_
