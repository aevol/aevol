// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_AA_2_MWH_VALUE_MAPPING_H_
#define AEVOL_AA_2_MWH_VALUE_MAPPING_H_

#include <cstdint>
#include <memory>

#include "AminoAcid.h"

namespace aevol_4b {

class AA_2_MWH_Value_Mapping {
 public:
  AA_2_MWH_Value_Mapping();
  AA_2_MWH_Value_Mapping(const AA_2_MWH_Value_Mapping &)            = delete;
  AA_2_MWH_Value_Mapping(AA_2_MWH_Value_Mapping &&)                 = delete;
  AA_2_MWH_Value_Mapping &operator=(const AA_2_MWH_Value_Mapping &) = delete;
  AA_2_MWH_Value_Mapping &operator=(AA_2_MWH_Value_Mapping &&)      = delete;
  virtual ~AA_2_MWH_Value_Mapping()                                 = default;

  auto aa_base_m() const { return aa_base_m_; }
  auto aa_base_w() const { return aa_base_w_; }
  auto aa_base_h() const { return aa_base_h_; }

  auto aa_base_m_size() const { return aa_base_m_size_; }
  auto aa_base_w_size() const { return aa_base_w_size_; }
  auto aa_base_h_size() const { return aa_base_h_size_; }

  void reset_aa_base_m(const int8_t *base_m);
  void reset_aa_base_w(const int8_t *base_w);
  void reset_aa_base_h(const int8_t *base_h);

 protected:
  int8_t aa_base_m_[NB_AMINO_ACIDS];
  int8_t aa_base_w_[NB_AMINO_ACIDS];
  int8_t aa_base_h_[NB_AMINO_ACIDS];

  int8_t aa_base_m_size_;
  int8_t aa_base_w_size_;
  int8_t aa_base_h_size_;
};

}  // namespace aevol_4b

#endif  // AEVOL_AA_2_MWH_VALUE_MAPPING_H_
