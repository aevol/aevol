// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_AMINOACID_H
#define AEVOL_AMINOACID_H

#include <cstdint>
#include <string>

#include "utils/utility.h"

namespace aevol_4b {

enum class AminoAcid : uint8_t {
  PHENYLALANINE = 0,
  LEUCINE       = 1,
  ISOLEUCINE    = 2,
  METHIONINE    = 3,
  VALINE        = 4,
  SERINE        = 5,
  PROLINE       = 6,
  THREONINE     = 7,
  ALANINE       = 8,
  TYROSINE      = 9,
  STOP          = 10,
  HISTIDINE     = 11,
  GLUTAMINE     = 12,
  ASPARAGINE    = 13,
  LYSINE        = 14,
  ASPARTIC_ACID = 15,
  GLUTAMIC_ACID = 16,
  CYSTEINE      = 17,
  TRYPTOPHAN    = 18,
  ARGININE      = 19,
  GLYCINE       = 20
};

static constexpr auto NB_AMINO_ACIDS = std::to_underlying(AminoAcid::GLYCINE) + 1;

inline std::string to_string(const AminoAcid& ff);

std::string to_string(const AminoAcid& aa) {
  switch (aa) {
    case AminoAcid::PHENYLALANINE : return "PHENYLALANINE";
    case AminoAcid::LEUCINE :       return "LEUCINE";
    case AminoAcid::ISOLEUCINE :    return "ISOLEUCINE";
    case AminoAcid::METHIONINE :    return "METHIONINE";
    case AminoAcid::VALINE :        return "VALINE";
    case AminoAcid::SERINE :        return "SERINE";
    case AminoAcid::PROLINE :       return "PROLINE";
    case AminoAcid::THREONINE :     return "THREONINE";
    case AminoAcid::ALANINE :       return "ALANINE";
    case AminoAcid::TYROSINE :      return "TYROSINE";
    case AminoAcid::STOP :          return "STOP";
    case AminoAcid::HISTIDINE :     return "HISTIDINE";
    case AminoAcid::GLUTAMINE :     return "GLUTAMINE";
    case AminoAcid::ASPARAGINE :    return "ASPARAGINE";
    case AminoAcid::LYSINE :        return "LYSINE";
    case AminoAcid::ASPARTIC_ACID : return "ASPARTIC_ACID";
    case AminoAcid::GLUTAMIC_ACID : return "GLUTAMIC_ACID";
    case AminoAcid::CYSTEINE :      return "CYSTEINE";
    case AminoAcid::TRYPTOPHAN :    return "TRYPTOPHAN";
    case AminoAcid::ARGININE :      return "ARGININE";
    case AminoAcid::GLYCINE :       return "GLYCINE";
  }
}

}  // namespace aevol_4b

#endif  // AEVOL_AMINOACID_H
