// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "CheckpointHandler.h"

#include <format>
#include <regex>
#include <sstream>

#include "aevol_flavor.h"
#include "aevol_version.h"
#include "io/fasta/fasta.h"
#include "Grid.h"
#include "Individual.h"

namespace aevol {

CheckpointHandler::CheckpointHandler(const std::filesystem::path& dir): dir_(dir) {
  std::filesystem::create_directories(dir_);
}

auto CheckpointHandler::read_checkpoint(aevol::time_type time) const
    -> std::tuple<
        indivs_idxs_map_t,
        Grid,
        std::unique_ptr<PhenotypicTarget>,
        std::unique_ptr<ExpSetup>,
        time_type,
        std::optional<time_type>,
        std::optional<time_type>,
        std::optional<time_type>> {
  // Open all files constitutive of a checkpoint
  auto chkp_path = make_chkp_path(time);
  auto checkpoint = mxifstream(chkp_path);
  if (not checkpoint) {
    std::cerr << "Error: failed to open file " << chkp_path << std::endl;
    exit(1);
  }
  auto setup_path = make_setup_path(time);
  auto setup_file = std::ifstream(setup_path);
  if (not setup_file) {
    std::cerr << "Error: failed to open file " << setup_path << std::endl;
    exit(1);
  }
  try {
    auto pop_path = make_pop_path(time);
    auto pop_file = FastaReader(pop_path);

    // Read header in all checkpoint-constitutive files
    std::string header;
    getline(checkpoint >> std::ws, header);
    check_compatibility(header, chkp_path);
    getline(pop_file >> std::ws, header);
    check_compatibility(header, pop_path);
    getline(setup_file >> std::ws, header);
    check_compatibility(header, setup_path);

    // Read grid size from checkpoint and pop size from population file; check consistency
    Grid::xy_type width, height;
    get_expected_or_throw(checkpoint, "Grid_size:", width, height);
    Grid::xy_type pop_size;
    get_expected_or_throw(pop_file, ";");
    get_expected_or_throw(pop_file, "Pop_size:", pop_size);
    if (pop_size != width * height) {
      std::cerr << "Error: checkpoint pop size and grid size do not match" << std::endl;
      exit(1);
    }

    // Read experiment setup
    auto exp_setup_read = ExpSetup::make_from_checkpoint(setup_file, checkpoint, pop_size);

    // Read grid
    auto grid = Grid(width, height);
    for (size_t i = 0 ; i < pop_size; ++i) {
      get_expected_or_throw(checkpoint, "GridCell:", grid[i]);
    }

    // Read phenotypic target
    auto phenotypic_target = PhenotypicTarget::make_from_checkpoint(
        setup_file, checkpoint, exp_setup_read->fuzzy_factory().flavor(), exp_setup_read->fuzzy_factory().sampling());

    // Read checkpoint frequency
    auto checkpoint_frequency = time_type{};
    get_expected_or_throw(setup_file, "CheckpointFrequency:", checkpoint_frequency);

    // Read tree output frequency
    auto tree_output_frequency = std::optional<time_type>{};
    get_expected_or_throw(checkpoint, "TreeOutputFrequency:", tree_output_frequency);

    // Read stats output frequencies
    auto best_indiv_stats_output_frequency = std::optional<time_type>{};
    auto whole_pop_stats_output_frequency = std::optional<time_type>{};
    get_expected_or_throw(setup_file, "BestIndivStatsOutputFrequency:", best_indiv_stats_output_frequency);
    get_expected_or_throw(setup_file, "PopStatsOutputFrequency:", whole_pop_stats_output_frequency);

    // Read population
    auto indivs_idxs_map = read_indivs_idxs_map(pop_file, pop_size);

    return std::make_tuple(indivs_idxs_map,
                           std::move(grid),
                           std::move(phenotypic_target),
                           std::move(exp_setup_read),
                           checkpoint_frequency,
                           tree_output_frequency,
                           best_indiv_stats_output_frequency,
                           whole_pop_stats_output_frequency);
  }
  catch (std::exception& e) {
    std::cerr << "Error while reading checkpoint:\n  " << e.what() << std::endl;
    exit(1);
  }
}

void CheckpointHandler::write_checkpoint(
    aevol::time_type time,
    const indivs_idxs_sorted_map_t& indivs_idxs_map,
    const Grid& grid,
    const PhenotypicTarget& target,
    const ExpSetup& exp_setup,
    time_type checkpoint_frequency,
    std::optional<time_type> tree_output_frequency,
    std::optional<time_type> best_indiv_stats_output_frequency,
    std::optional<time_type> whole_pop_stats_output_frequency) {
  // Open all files constitutive of a checkpoint
  auto chkp_path = make_chkp_path(time);
  auto checkpoint = mxofstream(chkp_path);
  if (not checkpoint) {
    std::cerr << "Error: failed to open file " << chkp_path << std::endl;
    exit(1);
  }
  auto pop_path = make_pop_path(time);
  auto pop_file = std::ofstream(pop_path);
  if (not pop_file) {
    std::cerr << "Error: failed to open file " << pop_path << std::endl;
    exit(1);
  }
  auto setup_path = make_setup_path(time);
  auto setup_file = std::ofstream(setup_path);
  if (not setup_file) {
    std::cerr << "Error: failed to open file " << setup_path << std::endl;
    exit(1);
  }

  // Write checkpoint header in all checkpoint files
  checkpoint << std::format("Checkpoint file generated by {} version {}\n", aevol::flavor, aevol::version);
  pop_file << std::format("; Population file generated by {} version {}\n", aevol::flavor, aevol::version);
  setup_file << std::format("Setup file generated by {} version {}\n", aevol::flavor, aevol::version);

  // Write grid size in checkpoint and pop size in population file
  checkpoint << "Grid_size: " << grid.width() << " " << grid.height() << "\n";
  pop_file << "; Pop_size: " << grid.width() * grid.height() << "\n";

  // Write exp_setup
  exp_setup.write_to_checkpoint(setup_file, checkpoint);

  // Write grid
  for (size_t i = 0 ; i < grid.width() * grid.height() ; ++i) {
    checkpoint << "GridCell: " << grid[i] << "\n";
  }

  // Write phenotypic target
  target.write_to_checkpoint(setup_file, checkpoint);

  // Write checkpoint frequency
  setup_file << "CheckpointFrequency: " << checkpoint_frequency << "\n";

  // Write tree output frequency
  checkpoint << "TreeOutputFrequency: " << tree_output_frequency << "\n";

  // Write stats output frequencies
  setup_file << "BestIndivStatsOutputFrequency: " << best_indiv_stats_output_frequency << "\n";
  setup_file << "PopStatsOutputFrequency: " << whole_pop_stats_output_frequency << "\n";

  // Write population
  write_indivs_idxs_map(pop_file, indivs_idxs_map);
}

auto CheckpointHandler::make_chkp_path(time_type t) const -> std::filesystem::path {
  return dir_ / std::format("checkpoint_{:0>9}.ae", t);
}

auto CheckpointHandler::make_pop_path(time_type t) const -> std::filesystem::path {
  return dir_ / std::format("population_{:0>9}.fa", t);
}

auto CheckpointHandler::make_setup_path(time_type t) const -> std::filesystem::path {
  return dir_ / std::format("setup_{:0>9}.txt", t);
}

void CheckpointHandler::check_compatibility(std::string header, std::filesystem::path path) const {
  if (header.find(aevol::flavor) == std::string::npos) {
    exit_with_usr_msg(std::format("discrepancy in aevol flavor:\n\trunning: {},\n\tfound in file: \"{}\" (file {})",
                                  aevol::flavor,
                                  header,
                                  path.string()));
  }

  std::regex version_regex("version ([0-9]+).([0-9]+)");
  std::smatch version_match;
  std::regex_search(header, version_match, version_regex);
  auto major = std::stoi(version_match[1]);
  auto minor = std::stoi(version_match[2]);

  if (major != aevol::version_major or minor != aevol::version_minor) {
    exit_with_usr_msg(std::format("incompatible aevol versions:\n\trunning: {},\n\tfound in file: \"{}\" (file {})",
                                  aevol::version,
                                  header,
                                  path.string()));
  }
}

auto CheckpointHandler::read_indiv_fasta(FastaReader& fasta_reader, std::size_t nb_indivs) const
    -> indivs_idxs_map_t::value_type {
  std::vector<std::string> chromosomes;
  indivs_idxs_map_t::mapped_type idxs;

  for (auto chrsm: chrsms) {
    auto [seqid, sequence, modifiers] = fasta_reader.read_sequence();

    chromosomes.push_back(sequence);

    if (chrsm == Chrsm::A) {
      auto idxs_str = modifiers["indexes"];

      if (idxs_str == "all") {
        for (auto idx = decltype(nb_indivs){0}; idx < nb_indivs; ++idx) {
          idxs.push_back(idx);
        }
      } else {
        auto sstream = std::istringstream(idxs_str);

        // Add each index in the index line to idxs
        auto idx = decltype(idxs)::value_type{};
        sstream >> idx;
        while (sstream) {
          idxs.push_back(idx);
          sstream >> idx;
        }
      }
    }
  }

  // ----- Create Individual object and map it to the corresponding indices into indivs_idxs_map
  auto indiv = exp_setup->diploid()
      ? Individual::make_from_sequences(chromosomes[0], chromosomes[1])
      : Individual::make_from_sequence(chromosomes.front());
  return std::make_pair(std::move(indiv), idxs);
}

void CheckpointHandler::write_indiv_fasta(std::ofstream& os,
                                          size_t genome_idx,
                                          const indivs_idxs_sorted_map_t::value_type& entry,
                                          bool clonal_population) const {
  for (auto chrsm: chrsms) {
    // ----- Write fasta definition line
    os << make_fasta_definition_line(time(), genome_idx, chrsm) << " [indexes=";
    if (clonal_population) {
      os << "all";
    } else {
      os << entry.second[0];
      for (size_t i = 1; i < entry.second.size(); ++i) {
        os << " " << entry.second[i];
      }
    }
    os << "]\n";

    // ----- Write sequence
    os << entry.first->annotated_chromosome(chrsm).dna() << "\n";
  }
}

auto CheckpointHandler::read_indivs_idxs_map(FastaReader& fasta_reader, std::size_t nb_indivs) const
    -> indivs_idxs_map_t {
  auto nb_retrieved_indivs = std::size_t(0);
  auto indivs_idxs_map = indivs_idxs_map_t{};

  while (nb_retrieved_indivs < nb_indivs) {
    auto entry = read_indiv_fasta(fasta_reader, nb_indivs);
    nb_retrieved_indivs += entry.second.size();
    indivs_idxs_map.insert(entry);
  }

  return indivs_idxs_map;
}

void CheckpointHandler::write_indivs_idxs_map(std::ofstream& os,
                                              const indivs_idxs_sorted_map_t& indivs_idxs_map) const {
  auto genome_idx = 0u;
  for (const auto& entry : indivs_idxs_map) {
    write_indiv_fasta(os, ++genome_idx, entry, indivs_idxs_map.size() == 1); // population is clonal if there's
                                                                             // only 1 sequence in map
  }
}

}  // namespace aevol
