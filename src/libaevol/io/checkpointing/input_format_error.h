// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_INPUT_FORMAT_ERROR_H_
#define AEVOL_INPUT_FORMAT_ERROR_H_

#include <stdexcept>
#include <string>

namespace aevol {

class input_format_error : public std::runtime_error {
 public:
  input_format_error(const std::string& expected, const std::string& found)
      : std::runtime_error("expected \"" + expected + "\", found \"" + found + "\"\n") {}
  input_format_error(char expected, char found)
      : std::runtime_error(std::string("expected \'") + expected + "\', found \'" + found + "\'\n") {}
};

}  // namespace aevol

#endif  // AEVOL_INPUT_FORMAT_ERROR_H_
