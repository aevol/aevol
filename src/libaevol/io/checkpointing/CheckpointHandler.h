// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_CHECKPOINTHANDLER_H_
#define AEVOL_CHECKPOINTHANDLER_H_

#include <filesystem>
#include <fstream>
#include <iostream>
#include <optional>
#include <string>

#include "AeTime.h"
#include "ae_types.h"
#include "ExpSetup.h"
#include "Grid.h"
#include "Individual.h"
#include "indiv_idxs_map.h"
#include "io/fasta/FastaReader.h"
#include "mxifstream.h"
#include "mxofstream.h"
#include "phenotype/PhenotypicTarget.h"

namespace aevol {

class CheckpointHandler {
 public:
  CheckpointHandler()                                    = delete;
  CheckpointHandler(const CheckpointHandler&)            = delete;
  CheckpointHandler(CheckpointHandler&&)                 = delete;
  CheckpointHandler& operator=(const CheckpointHandler&) = delete;
  CheckpointHandler& operator=(CheckpointHandler&&)      = delete;
  CheckpointHandler(const std::filesystem::path& dir);
  virtual ~CheckpointHandler()                           = default;

  auto read_checkpoint(aevol::time_type time) const
      -> std::tuple<
          indivs_idxs_map_t,
          Grid,
          std::unique_ptr<PhenotypicTarget>,
          std::unique_ptr<ExpSetup>,
          time_type,
          std::optional<time_type>,
          std::optional<time_type>,
          std::optional<time_type>>;
  void write_checkpoint(aevol::time_type time,
                        const indivs_idxs_sorted_map_t& indivs_idxs_map,
                        const Grid& grid,
                        const PhenotypicTarget& target,
                        const ExpSetup& exp_setup,
                        time_type checkpoint_frequency,
                        std::optional<time_type> tree_output_frequency,
                        std::optional<time_type> best_indiv_stats_output_frequency,
                        std::optional<time_type> whole_pop_stats_output_frequency);

 protected:
  std::filesystem::path dir_;

  auto make_chkp_path(time_type t) const -> std::filesystem::path;
  auto make_pop_path(time_type t) const -> std::filesystem::path;
  auto make_setup_path(time_type t) const -> std::filesystem::path;

  void check_compatibility(std::string header, std::filesystem::path path) const;

  auto read_indiv_fasta(FastaReader& fasta_reader, std::size_t nb_indivs) const -> indivs_idxs_map_t::value_type;
  void write_indiv_fasta(std::ofstream& os,
                         size_t genome_idx,
                         const indivs_idxs_sorted_map_t::value_type& entry,
                         bool clonal_population) const;

  auto read_indivs_idxs_map(FastaReader& fasta_reader, std::size_t nb_indivs) const -> indivs_idxs_map_t;
  void write_indivs_idxs_map(std::ofstream& os, const indivs_idxs_sorted_map_t& indivs_idxs_map) const;
};

}  // namespace aevol

#endif  // AEVOL_CHECKPOINTHANDLER_H_
