// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_MXIFSTREAM_H_
#define AEVOL_MXIFSTREAM_H_

#include <filesystem>
#include <fstream>
#include <optional>

#include "input_format_error.h"

namespace aevol {

class mxifstream {
 public:
  mxifstream()                             = delete;
  mxifstream(const mxifstream&)            = delete;
  mxifstream(mxifstream&&)                 = delete;
  mxifstream& operator=(const mxifstream&) = delete;
  mxifstream& operator=(mxifstream&&)      = delete;
  virtual ~mxifstream();

  mxifstream(const std::filesystem::path& filename);

  template<class T>
  mxifstream& operator>>(T& v);
  mxifstream& operator>>(std::floating_point auto& v);
  template<class T>
  mxifstream& operator>>(std::optional<T>& ov);
  mxifstream& operator>>(std::istream& (*manip_fn)(std::istream&));

  auto get() { return in_.get(); }
  void get_expected_or_throw(char expected_char);
  mxifstream& read(char* s, std::streamsize count);

  bool fail() const { return in_.fail(); };
  explicit operator bool() const { return !fail(); };

 protected:
  std::ifstream in_;

  // Friend declarations
  template<class CharT, class Traits, class Allocator>
  friend mxifstream& getline(mxifstream& stream, std::basic_string<CharT, Traits, Allocator>& str );
};

template<class T>
mxifstream& mxifstream::operator>>(T& v) {
  in_ >> v;
  return *this;
}

mxifstream& mxifstream::operator>>(std::floating_point auto& v) {
  in_ >> std::ws;
  get_expected_or_throw('<');
  in_.read(reinterpret_cast<char*>(&v), sizeof(v));
  get_expected_or_throw('>');
  return *this;
}

template<class T>
mxifstream& mxifstream::operator>>(std::optional<T>& ov) {
  auto has_value = bool{};
  in_ >> has_value;
  if (has_value) {
    auto v = T{};
    in_ >> v;
    ov = v;
  }
  return *this;
}

inline mxifstream& mxifstream::operator>>(std::istream& (*manip_fn)(std::istream&)) {
  in_ >> manip_fn;
  return *this;
}

template< class CharT, class Traits, class Allocator >
mxifstream& getline(mxifstream& stream, std::basic_string<CharT, Traits, Allocator>& str ) {
  getline(stream.in_, str);
  return stream;
}

}  // namespace aevol

#endif  // AEVOL_MXIFSTREAM_H_
