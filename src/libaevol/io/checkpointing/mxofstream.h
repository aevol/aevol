// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_MXOFSTREAM_H_
#define AEVOL_MXOFSTREAM_H_

#include <concepts>
#include <filesystem>
#include <fstream>
#include <optional>

namespace aevol {

class mxofstream {
 public:
  mxofstream()                             = delete;
  mxofstream(const mxofstream&)            = delete;
  mxofstream(mxofstream&&)                 = delete;
  mxofstream& operator=(const mxofstream&) = delete;
  mxofstream& operator=(mxofstream&&)      = delete;
  virtual ~mxofstream();

  mxofstream(const std::filesystem::path& filename);

  template<class T>
  mxofstream& operator<<(const T& v);
  mxofstream& operator<<(const std::floating_point auto& v);
  template<class T>
  mxofstream& operator<<(const std::optional<T>& ov);
  mxofstream& operator<<(std::ostream& (*manip_fn)(std::ostream&));

  mxofstream& write(const char* s, std::streamsize count);

  bool fail() const { return out_.fail(); };
  explicit operator bool() const { return !fail(); };

 protected:
  std::ofstream out_;
};

template<class T>
mxofstream& mxofstream::operator<<(const T& v) {
  out_ << v;
  return *this;
}

template<class T>
mxofstream& mxofstream::operator<<(const std::optional<T>& ov) {
  out_ << ov.has_value();
  if (ov) out_ << ' ' << ov.value();
  return *this;
}

mxofstream& mxofstream::operator<<(const std::floating_point auto& v) {
  out_ << '<';
  out_.write(reinterpret_cast<const char*>(&v), sizeof(v));
  out_ << '>';
  return *this;
}

inline mxofstream& mxofstream::operator<<(std::ostream& (*manip_fn)(std::ostream&)) {
  out_ << manip_fn;
  return *this;
}

}  // namespace aevol

#endif  // AEVOL_MXOFSTREAM_H_
