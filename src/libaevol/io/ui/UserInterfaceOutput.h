// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_USERINTERFACEOUTPUT_H_
#define AEVOL_USERINTERFACEOUTPUT_H_

#include <cinttypes>
#include <filesystem>

#include "Individual.h"
#include "PhenotypicTarget.h"
#include "Population.h"

namespace aevol {

class UserInterfaceOutput {
 public:
  UserInterfaceOutput()                                      = delete;
  UserInterfaceOutput(const UserInterfaceOutput&)            = delete;
  UserInterfaceOutput(UserInterfaceOutput&&)                 = delete;
  UserInterfaceOutput& operator=(const UserInterfaceOutput&) = delete;
  UserInterfaceOutput& operator=(UserInterfaceOutput&&)      = delete;
  UserInterfaceOutput(const std::filesystem::path& outdir, int32_t frequency);
  virtual ~UserInterfaceOutput()                             = default;

  bool nothing_to_output() const;
  void write_indiv_output(const Individual& indiv);
  void write_grid_output(size_t grid_width, size_t grid_height, const Population& population);
  void write_environment_output(const PhenotypicTarget& target);

 protected:
  std::filesystem::path outdir_;
  int32_t frequency_;
};

}

#endif  // AEVOL_USERINTERFACEOUTPUT_H_
