// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_FASTA_READER_H_
#define AEVOL_FASTA_READER_H_

#include <filesystem>
#include <fstream>

#include "fasta.h"

namespace aevol {

class FastaReader {
 public:
  FastaReader()                              = delete;
  FastaReader(const FastaReader&)            = delete;
  FastaReader(FastaReader&&)                 = delete;
  FastaReader& operator=(const FastaReader&) = delete;
  FastaReader& operator=(FastaReader&&)      = delete;
  virtual ~FastaReader()                     = default;

  FastaReader(std::filesystem::path filename);

  template<class T>
  FastaReader& operator>>(T& v);
  FastaReader& operator>>(std::istream& (*manip_fn)(std::istream&));
  auto eof() const { return in_.eof(); }

  auto read_sequence() noexcept(false) { return read_fasta_sequence(in_, filename_); };
  auto seqid_to_generation(std::string seqid) const -> time_type;

 protected:
  std::filesystem::path filename_;
  std::ifstream in_;

  // Friend declarations
  template<class CharT, class Traits, class Allocator>
  friend FastaReader& getline(FastaReader& stream, std::basic_string<CharT, Traits, Allocator>& str );
};

template<class T>
FastaReader& FastaReader::operator>>(T& v) {
  in_ >> v;
  return *this;
}

inline FastaReader& FastaReader::operator>>(std::istream& (*manip_fn)(std::istream&)) {
  in_ >> manip_fn;
  return *this;
}

template< class CharT, class Traits, class Allocator >
FastaReader& getline(FastaReader& stream, std::basic_string<CharT, Traits, Allocator>& str ) {
  getline(stream.in_, str);
  return stream;
}

}  // namespace aevol

#endif  // AEVOL_FASTA_READER_H_
