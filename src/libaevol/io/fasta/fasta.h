// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_FASTA_H_
#define AEVOL_FASTA_H_

#include <format>
#include <fstream>
#include <map>
#include <regex>
#include <stdexcept>
#include <string>
#include <tuple>

#include "ae_types.h"
#include "aevol_flavor.h"
#include "Chrsm.h"
#include "ExpSetup.h"

namespace aevol {

class fasta_error : public std::runtime_error {
 public:
  fasta_error(const std::string& str): std::runtime_error(str) {}
};

class non_aevol_fasta_error : public fasta_error {
 public:
  non_aevol_fasta_error(const std::string& str): fasta_error(str) {}
};

auto make_fasta_definition_line(aevol::time_type time, size_t id, Chrsm chrsm = Chrsm::A) -> std::string;
auto read_fasta_sequence(std::ifstream& is, const std::string& file_name) noexcept(false)
    -> std::tuple<std::string, std::string, std::map<std::string, std::string>>;
auto read_fasta_file(const std::string& file_name) noexcept(false) -> std::vector<std::string>;

inline auto make_fasta_definition_line(aevol::time_type time, size_t id, Chrsm chrsm) -> std::string {
  auto str = std::format(">Seq{}-{}", time, id);
  if (exp_setup->diploid()) {
    str += "-" + to_string(chrsm);
  }
  return str + std::format(" [organism={}]", aevol::flavor);
}

}  // namespace aevol

#endif  // AEVOL_FASTA_H_
