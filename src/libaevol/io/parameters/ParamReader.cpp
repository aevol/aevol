// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************




// =================================================================
//                              Libraries
// =================================================================
#include <cassert>
#include <charconv>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <list>
#include <string>

#include "SelectionScheme.h"
#include "SelectionScope.h"
#include "utils/utility.h"

#ifdef BASE_4
  #include "aevol_4b/biochemistry/AminoAcid.h"
#endif

// =================================================================
//                            Project Files
// =================================================================
#include "ParamReader.h"

#include "Gaussian.h"

namespace aevol {

// =================================================================
//                          Class declarations
// =================================================================


//##############################################################################
//                                                                             #
//                             Class ParamReader                               #
//                                                                             #
//##############################################################################

// =================================================================
//                    Definition of static attributes
// =================================================================
const char kTabChar = 0x09;

// =================================================================
//                            Public Methods
// =================================================================
auto ParamReader::read_file(const char* file_name) -> std::unique_ptr<ParamValues> {
  auto param_values = std::make_unique<ParamValues>();

  // Open parameter file
  FILE* file = fopen(file_name,  "r");

  if (file == NULL) {
    printf("ERROR : couldn't open file %s\n", file_name);
    exit(EXIT_FAILURE);
  }

  int32_t cur_line = 0;
  ParameterLine* parameter_line;

  // TODO : write parameter_line = new ParameterLine(param_file_) => ParameterLine::ParameterLine(char*)
  while ((parameter_line = line(file, cur_line)) != NULL) {
    interpret_line(*parameter_line, cur_line, file_name, *param_values);
    delete parameter_line;
  }

  fclose(file);

  // Check consistency of parameter values
  check_consistency(*param_values);

  return param_values;
}

// =================================================================
//                           Protected Methods
// =================================================================
void ParamReader::check_consistency(ParamValues& params) {
  if (params.chromosome_maximal_length == -1)
    params.chromosome_maximal_length = params.max_genome_length;
  if (params.chromosome_minimal_length == -1)
    params.chromosome_minimal_length = params.min_genome_length;
  if (params.chromosome_minimal_length > params.chromosome_initial_length) {
    printf("ERROR: CHROMOSOME_INITIAL_LENGTH is lower than CHROMOSOME_MINIMAL_LENGTH\n");
    exit(EXIT_FAILURE);
  }
  if (params.chromosome_maximal_length < params.chromosome_initial_length) {
    printf("ERROR: CHROMOSOME_INITIAL_LENGTH is higher than CHROMOSOME_MAXIMAL_LENGTH\n");
    exit(EXIT_FAILURE);
  }
}

/*!
  \brief Get a line in a file and format it

  \return line (pointer)

  \see format_line(ParameterLine* formated_line, char* line, bool* line_is_interpretable)
*/
ParameterLine* ParamReader::line(FILE* file, int32_t& cur_line) {
  char line[4096];
  ParameterLine* formated_line = new ParameterLine();

  bool found_interpretable_line = false; // Found line that is neither a comment nor empty

  while (!feof(file) && !found_interpretable_line)
  {
    if (!fgets(line, 4096, file))
    {
      delete formated_line;
      return NULL;
    }
    cur_line++;
    format_line(formated_line, line, &found_interpretable_line);
  }

  if (found_interpretable_line)
  {
    return formated_line;
  }
  else
  {
    delete formated_line;
    return NULL;
  }
}

/*!
  \brief Format a line by parsing it and the words inside

  \param formated_line the resulted formated line
  \param line original line in char*
  \param line_is_interpretable
*/
void ParamReader::format_line(ParameterLine* formated_line,
                              char* line,
                              bool* line_is_interpretable) {
  int32_t i = 0;
  int32_t j;

  // Parse line
  while (line[i] != '\n' && line[i] != '\0' && line[i] != '\r')
  {
    j = 0;

    // Flush white spaces and tabs
    while (line[i] == ' ' || line[i] == kTabChar) i++;

    // Check comments
    if (line[i] == '#') break;

    // If we got this far, there is content in the line
    *line_is_interpretable = true;

    // Parse word
    while (line[i] != ' ' && line[i] != kTabChar && line[i] != '\n' &&
           line[i] != '\0' && line[i] != '\r') {
      formated_line->words[formated_line->nb_words][j++] = line[i++];
    }

    // Add '\0' at end of word if it's not empty (line ending with space or tab)
    if (j != 0)
    {
      formated_line->words[formated_line->nb_words++][j] = '\0';
    }
  }
}

void ParamReader::interpret_line(const ParameterLine& line,
                                 int32_t cur_line,
                                 const char* file_name,
                                 ParamValues& values) {
  if (strcmp(line.words[0], "STRAIN_NAME") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "MIN_TRIANGLE_WIDTH") == 0) {
    stale_option_error(file_name, cur_line, line.words[0], "its value is fixed to 0");
  }
  else if (strcmp(line.words[0], "MAX_TRIANGLE_WIDTH") == 0)
  {
    values.w_max = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "ENV_AXIS_FEATURES") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "ENV_SEPARATE_SEGMENTS") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "TREE_MODE") == 0)
  {
    printf("ERROR in param file \"%s\" on line %" PRId32 ": "
           "Tree mode management has been removed.\n",
           file_name, cur_line);
    exit(EXIT_FAILURE);
  }
  else if (strcmp(line.words[0], "RECORD_LIGHT_TREE") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "MORE_STATS") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "DUMP_PERIOD") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "DUMP_STEP") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "BACKUP_STEP") == 0) {
    stale_option_error(file_name, cur_line, line.words[0], "use CHECKPOINT_STEP instead");
  }
  else if (strcmp(line.words[0], "CHECKPOINT_STEP") == 0) {
    values.checkpoint_frequency = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "BIG_BACKUP_STEP") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "TREE_STEP") == 0) {
    stale_option_error(file_name, cur_line, line.words[0], "use RECORD_TREE instead");
  } else if (strcmp(line.words[0], "RECORD_TREE") == 0) {
    if (strncmp(line.words[1], "ON", 2) == 0) {
      auto val = decltype(values.tree_output_frequency)::value_type{};
      auto [ptr, ec] = std::from_chars(line.words[2], line.words[2] + strlen(line.words[2]), val);
      if (ec != std::errc()) {
        std::cout << "ERROR in param file: " << file_name << ':' << cur_line << ":\n"
                  << "\tinvalid value for option " << line.words[0] << "\n";
        exit(EXIT_FAILURE);
      }
      values.tree_output_frequency = val;
    } else if (strncmp(line.words[1], "OFF", 3) == 0) {
      values.tree_output_frequency = std::nullopt;
    } else {
      std::cout << "ERROR in param file: " << file_name << ':' << cur_line << ":\n"
                << "\tinvalid value for option " << line.words[0] << "\n";
      exit(EXIT_FAILURE);
    }
  } else if (strcmp(line.words[0], "STATS_BEST") == 0) {
    if (strncmp(line.words[1], "ON", 2) == 0) {
      auto val = decltype(values.best_indiv_stats_output_frequency)::value_type{};
      auto [ptr, ec] = std::from_chars(line.words[2], line.words[2] + strlen(line.words[2]), val);
      if (ec != std::errc()) {
        std::cout << "ERROR in param file: " << file_name << ':' << cur_line << ":\n"
                  << "\tinvalid value for option " << line.words[0] << "\n";
        exit(EXIT_FAILURE);
      }
      values.best_indiv_stats_output_frequency = val;
    } else if (strncmp(line.words[1], "OFF", 3) == 0) {
      values.best_indiv_stats_output_frequency = std::nullopt;
    } else {
      std::cout << "ERROR in param file: " << file_name << ':' << cur_line << ":\n"
                << "\tinvalid value for option " << line.words[0] << "\n";
      exit(EXIT_FAILURE);
    }
  } else if (strcmp(line.words[0], "STATS_POP") == 0) {
    if (strncmp(line.words[1], "ON", 2) == 0) {
      auto val = decltype(values.pop_stats_output_frequency)::value_type{};
      auto [ptr, ec] = std::from_chars(line.words[2], line.words[2] + strlen(line.words[2]), val);
      if (ec != std::errc()) {
        std::cout << "ERROR in param file: " << file_name << ':' << cur_line << ":\n"
                  << "\tinvalid value for option " << line.words[0] << "\n";
        exit(EXIT_FAILURE);
      }
      values.pop_stats_output_frequency = val;
    } else if (strncmp(line.words[1], "OFF", 3) == 0) {
      values.pop_stats_output_frequency = std::nullopt;
    } else {
      std::cout << "ERROR in param file: " << file_name << ':' << cur_line << ":\n"
                << "\tinvalid value for option " << line.words[0] << "\n";
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "NB_GENER") == 0) {
    stale_option_error(file_name, cur_line, line.words[0], "use command line arguments of aevol_run instead");
  }
  else if (strcmp(line.words[0], "INITIAL_GENOME_LENGTH") == 0) {
    stale_option_error(file_name, cur_line, line.words[0], "use CHROMOSOME_INITIAL_LENGTH instead");
  }
  else if (strcmp(line.words[0], "CHROMOSOME_INITIAL_LENGTH") == 0)
  {
    values.chromosome_initial_length = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "MIN_GENOME_LENGTH") == 0)
  {
    if (strncmp(line.words[1], "NONE", 4) == 0)
    {
      values.min_genome_length = 1; // Must not be 0
    }
    else
    {
      values.min_genome_length = atol(line.words[1]);
      if (values.min_genome_length == 0)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32 " : MIN_GENOME_LENGTH must be > 0.\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }
    }
  }
  else if (strcmp(line.words[0], "MAX_GENOME_LENGTH") == 0)
  {
    if (strncmp(line.words[1], "NONE", 4) == 0)
    {
      values.max_genome_length = INT32_MAX;
    }
    else
    {
      values.max_genome_length = atol(line.words[1]);
    }
  }
  else if (strcmp(line.words[0], "INIT_POP_SIZE") == 0) {
    stale_option_error(file_name, cur_line, line.words[0], "use GRID_SIZE <width> <height> instead");
  }
  else if (strcmp(line.words[0], "WORLD_SIZE") == 0) {
    stale_option_error(file_name, cur_line, line.words[0], "use GRID_SIZE <width> <height> instead");
  }
  else if (strcmp(line.words[0], "GRID_SIZE") == 0) {
    values.grid_width   = atoi(line.words[1]);
    values.grid_height  = atoi(line.words[2]);
  }
  else if (strcmp(line.words[0], "POP_STRUCTURE") == 0) {
    stale_option_error(file_name, cur_line, line.words[0], "use GRID_SIZE <width> <height> instead");
  }
  else if (strcmp(line.words[0], "MIGRATION_NUMBER") == 0) {
    stale_option_error(file_name, cur_line, line.words[0],
                       "use INDIV_MIXING instead.\n\tusage: INDIV_MIXING WELL_MIXED|NONE|PARTIAL <n>");
  }
  else if (strcmp(line.words[0], "INDIV_MIXING") == 0)
  {
    if (strcmp(line.words[1], "WELL_MIXED") == 0)
      values.well_mixed = true;
    else if (strcmp(line.words[1], "NONE") == 0)
      values.well_mixed = false;
    else if (strcmp(line.words[1], "PARTIAL") == 0)
      values.partial_mix_nb_permutations = atol(line.words[2]);
    else {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown mixing option.\n", file_name, cur_line);
      printf("usage: INDIV_MIXING WELL_MIXED|NONE|PARTIAL <n>\n");
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "INIT_METHOD") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "POINT_MUTATION_RATE") == 0)
  {
    values.point_mutation_rate = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "SMALL_INSERTION_RATE") == 0)
  {
    values.small_insertion_rate = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "SMALL_DELETION_RATE") == 0)
  {
    values.small_deletion_rate = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "MAX_INDEL_SIZE") == 0)
  {
    values.max_indel_size = atol(line.words[1]);
  }
  else if (strcmp(line.words[0], "DUPLICATION_RATE") == 0)
  {
    values.duplication_rate = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "DELETION_RATE") == 0)
  {
    values.deletion_rate = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "TRANSLOCATION_RATE") == 0)
  {
    values.translocation_rate = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "INVERSION_RATE") == 0)
  {
    values.inversion_rate = atof(line.words[1]);
  }
  else if (strcmp(line.words[0], "DUPLICATION_PROPORTION") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "DELETION_PROPORTION") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "TRANSLOCATION_PROPORTION") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "INVERSION_PROPORTION") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "RECOMB_ON_ALIGN") == 0) {
    if (strncmp(line.words[1], "true", 4) == 0) {
      values.recomb_on_align_ = true;
    }
    else if (strncmp(line.words[1], "false", 5) == 0) {
      values.recomb_on_align_ = false;
    }
    else {
      std::cout << "ERROR in param file \"" << file_name << "\" on line " << cur_line <<
          ": unknown RECOMB_ON_ALIGN option (use true/false)." << std::endl;
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "ALIGN_FUNCTION") == 0) {
    if (strcmp(line.words[1], "LINEAR") == 0) {
      if (line.nb_words != 4) {
        std::cout << "ERROR in param file \"" << file_name << "\" on line " << cur_line <<
            ": wrong number of parameters" << std::endl;
        exit(EXIT_FAILURE);
      }
      auto min = align::score_t{};
      auto& min_str = line.words[2];
      std::from_chars(min_str, min_str + strlen(min_str), min);
      auto max = align::score_t{};
      auto& max_str = line.words[3];
      std::from_chars(max_str, max_str + strlen(max_str), max);
      values.align_score_function_ = align::ScoreFunction::make_linear_function(min, max);
    } else if (strcmp(line.words[1], "SIGMOID") == 0) {
      if (line.nb_words != 4) {
        std::cout << "ERROR in param file \"" << file_name << "\" on line " << cur_line <<
            ": wrong number of parameters" << std::endl;
        exit(EXIT_FAILURE);
      }
      auto mean = align::score_t{};
      auto& mean_str = line.words[2];
      std::from_chars(mean_str, mean_str + strlen(mean_str), mean);
      auto lambda = double{};
      auto& lambda_str = line.words[3];
      std::from_chars(lambda_str, lambda_str + strlen(lambda_str), lambda);
      values.align_score_function_ = align::ScoreFunction::make_sigmoid_function(mean, lambda);
    }
  }
  else if (strcmp(line.words[0], "ALIGN_MAX_SHIFT") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "ALIGN_W_ZONE_H_LEN") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "ALIGN_MATCH_BONUS") == 0) {
    auto& str = line.words[1];
    std::from_chars(str, str + strlen(str), values.align_match_bonus_);
  }
  else if (strcmp(line.words[0], "ALIGN_MISMATCH_COST") == 0) {
    auto& str = line.words[1];
    std::from_chars(str, str + strlen(str), values.align_mismatch_cost_);
  }
  else if (strcmp(line.words[0], "MAX_ALIGN_SIZE") == 0) {
    auto& str = line.words[1];
    std::from_chars(str, str + strlen(str), values.max_align_size_);
  }
  else if (strcmp(line.words[0], "NEIGHBOURHOOD_RATE") == 0) {
    if (line.nb_words != 2) {
      std::cout << "ERROR in param file \"" << file_name << "\" on line " << cur_line <<
          ": wrong number of parameters" << std::endl;
      exit(EXIT_FAILURE);
    }
    auto& str = line.words[1];
    std::from_chars(str, str + strlen(str), values.align_neighbourhood_rate_);
  }
  else if (strcmp(line.words[0], "STOCHASTICITY") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "SELECTION_SCHEME") == 0)
  {
    if (strncmp(line.words[1], "lin", 3) == 0)
    {
      if (line.nb_words != 3)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": selection pressure parameter is missing.\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }

      values.selection_scheme    = SelectionScheme::RANK_LINEAR;
      values.selection_pressure  = atof(line.words[2]);
    }
    else if (strncmp(line.words[1], "exp", 3) == 0)
    {
      if (line.nb_words != 3)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": selection pressure parameter is missing.\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }

      values.selection_scheme    = SelectionScheme::RANK_EXPONENTIAL;
      values.selection_pressure  = atof(line.words[2]);
    }
    else if (strncmp(line.words[1], "fitness", 7) == 0)
    {
      if (line.nb_words != 3)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": selection pressure parameter is missing.\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }

      values.selection_scheme    = SelectionScheme::FITNESS_PROPORTIONATE;
      values.selection_pressure  = atof(line.words[2]);
    }
    else if (strcmp(line.words[1], "fittest") == 0)
    {
      values.selection_scheme = SelectionScheme::FITTEST;
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown selection scheme \"%s\".\n",
             file_name, cur_line, line.words[1]);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "SELFING_CONTROL") == 0)
  {
    #ifdef SEX
    if (strncmp(line.words[1], "true", 4) == 0) {
      values.selfing_control_ = true;

      if (line.nb_words != 3) {
        std::cout << "ERROR in param file \"" << file_name << "\" on line " << cur_line <<
               ": selfing rate parameter is missing." << std::endl;
        exit(EXIT_FAILURE);
      }

      const auto& selfing_rate_str = line.words[2];
      std::from_chars(selfing_rate_str, selfing_rate_str + strlen(selfing_rate_str), values.selfing_rate_);

      if (values.selfing_rate_ < 0 || values.selfing_rate_ > 1) {
        std::cout << "ERROR in param file \"" << file_name << "\" on line " << cur_line <<
                     ": selfing rate must be in [0, 1]." << std::endl;
        exit(EXIT_FAILURE);
      }
    }
    else if (strncmp(line.words[1], "false", 5) == 0) {
      values.selfing_control_ = false;
    }
    else {
      std::cout << "ERROR in param file \"" << file_name << "\" on line " << cur_line <<
             ": unknown selfing control option (use true/false)." << std::endl;
      exit(EXIT_FAILURE);
    }
    #else // SEX
    std::cout << "ERROR in param file \"" << file_name << "\" on line " << cur_line <<
        ": selfing is only valid in the eukaryote setting." << std::endl;
    exit(EXIT_FAILURE);
    #endif // SEX
  }
  else if (strcmp(line.words[0], "SELECTION_SCOPE") == 0)
  {
    if (strncmp(line.words[1], "global", 6) == 0)
    {
      values.selection_scope = SelectionScope::GLOBAL;
    }
    else if (strncmp(line.words[1], "local", 5) == 0)
    {
      if (line.nb_words != 4)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32
                   ": selection scope parameter for local selection is missing (x,y).\n",
               file_name, cur_line);
        exit(EXIT_FAILURE);
      }

      values.selection_scope    = SelectionScope::LOCAL;
      values.selection_scope_x  = atoi(line.words[2]);
      values.selection_scope_y  = atoi(line.words[2]);
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": unknown selection scope \"%s\".\n",
             file_name, cur_line, line.words[1]);
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "SEED") == 0)
  {
    static bool seed_already_set = false;
    if (seed_already_set)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": duplicate entry for SEED.\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
    values.seed      = atol(line.words[1]);
    seed_already_set = true;
  }
  else if (strcmp(line.words[0], "MUT_SEED") == 0)
  {
    static bool mut_seed_already_set = false;
    if (mut_seed_already_set)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": duplicate entry for MUT_SEED.\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
    values.mut_seed      = atol(line.words[1]);
    mut_seed_already_set = true;
  }
  else if (strcmp(line.words[0], "STOCH_SEED") == 0)
  {
    static bool stoch_seed_already_set = false;
    if (stoch_seed_already_set)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32
                 ": duplicate entry for STOCH_SEED.\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }
    values.stoch_seed      = atol(line.words[1]);
    stoch_seed_already_set = true;
  }
  else if (strcmp(line.words[0], "WITH_4PTS_TRANS") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "WITH_ALIGNMENTS") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "WITH_TRANSFER") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "REPL_TRANSFER_WITH_CLOSE_POINTS") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "SWAP_GUS") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "TRANSFER_INS_RATE") == 0){
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "TRANSFER_REPL_RATE") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "REPL_TRANSFER_DETACH_RATE") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "TRANSLATION_COST") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "ENV_ADD_POINT") == 0)
  {
    // custom_points
    printf("ERROR in param file \"%s\" on line %" PRId32
               ": Custom points management has been removed.\n",
        file_name, cur_line);
    exit(EXIT_FAILURE);
  }
  else if ((strcmp(line.words[0], "ENV_ADD_GAUSSIAN") == 0) ||
      (strcmp(line.words[0], "ENV_GAUSSIAN") == 0))
  {
    values.std_env_gaussians.push_back(Gaussian(atof(line.words[1]), atof(line.words[2]), atof(line.words[3])));
  }
  else if (strcmp(line.words[0], "ENV_SAMPLING") == 0)
  {
    values.env_sampling = atoi(line.words[1]);
  }
  else if (strcmp(line.words[0], "ENV_VARIATION") == 0) {
    if (strcmp(line.words[1], "none") != 0) {
      std::cout << "ERROR in param file: " << file_name << ':' << cur_line << ":\n"
                << "\t" << line.words[0] << " option is currently not available.\n"
                << "\t" << "Please use \"none\" or remove line from parameter file.\n";
      exit(EXIT_FAILURE);
    }
  }
  else if (strcmp(line.words[0], "ENV_NOISE") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "SECRETION_FITNESS_CONTRIB") == 0) {
    stale_option_error(file_name, cur_line, line.words[0], "use SECRETION_CONTRIB_TO_FITNESS instead");
  }
  else if (strcmp(line.words[0], "SECRETION_CONTRIB_TO_FITNESS") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "SECRETION_DIFFUSION_PROP") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "SECRETION_DEGRADATION_PROP") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "SECRETION_INITIAL") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "SECRETION_COST") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "ALLOW_PLASMIDS") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "PLASMID_INITIAL_LENGTH") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "PLASMID_INITIAL_GENE") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "PLASMID_MINIMAL_LENGTH") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "PLASMID_MAXIMAL_LENGTH") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "CHROMOSOME_MINIMAL_LENGTH") == 0)
  {
    values.chromosome_minimal_length = atoi(line.words[1]);
  }
  else if (strcmp(line.words[0], "CHROMOSOME_MAXIMAL_LENGTH") == 0)
  {
    values.chromosome_maximal_length = atoi(line.words[1]);
  }
  else if (strcmp(line.words[0], "PROB_HORIZONTAL_TRANS") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "PROB_PLASMID_HT") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "TUNE_DONOR_ABILITY") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "TUNE_RECIPIENT_ABILITY") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "DONOR_COST") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "RECIPIENT_COST") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "COMPUTE_PHEN_CONTRIB_BY_GU") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "LOG") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  else if (strcmp(line.words[0], "FUZZY_FLAVOR") == 0)
  {
    if (strcmp(line.words[1], "VECTOR") == 0) {
      values.fuzzy_flavor = FuzzyFlavor::VECTOR;
    }
    else if (strcmp(line.words[1], "DISCRETE_DOUBLE_TABLE") == 0) {
      values.fuzzy_flavor = FuzzyFlavor::DISCRETE_DOUBLE_TABLE;
    }
    else {
      printf("ERROR in param file \"%s\" on line %" PRId32 " : unknown fuzzy flavor %s.\n",
             file_name, cur_line, line.words[1]);
      exit(EXIT_FAILURE);
    }
  }  else if (strcmp(line.words[0], "SIMD_METADATA_FLAVOR") == 0) {
    stale_option_error(file_name, cur_line, line.words[0]);
  }
  #ifdef BASE_4
else if(strcmp(line.words[0], "AMINO_ACID") == 0)
  {
    if(!values.mwh_bases_redefined)
    {
      for(auto i = 0; i < aevol_4b::NB_AMINO_ACIDS; i++)
      {
        values.aa_base_m[i] = (int8_t) -1;
        values.aa_base_w[i] = (int8_t) -1;
        values.aa_base_h[i] = (int8_t) -1;
      }

      values.mwh_bases_redefined = true;
    }

    if(line.nb_words < 3)
    {
      printf("ERROR in param file \"%s\" on line %" PRId32 " : amino-acid base definition should at least comprise one amino-acid and one base digit\n",
             file_name, cur_line);
      exit(EXIT_FAILURE);
    }

    aevol_4b::AminoAcid amino_acid;
    if(values.str_to_aminoacid.find(std::string(line.words[1])) != values.str_to_aminoacid.end())
    {
      amino_acid = values.str_to_aminoacid.at(std::string(line.words[1]));
    }
    else
    {
      printf("ERROR in param file \"%s\" on line %" PRId32 " : unrecognized amino-acid string : %s\n",
             file_name, cur_line, line.words[1]);
      exit(EXIT_FAILURE);
    }

    for(auto i = 2; i < line.nb_words; i++)
    {
      if(strlen(line.words[i]) < 2)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32 " : malformed base digit : %s\n",
               file_name, cur_line, line.words[i]);
        exit(EXIT_FAILURE);
      }

      int8_t digit;

      try {
        digit = (int8_t) std::stoi(std::string(
            &(line.words[i][1])
        ));
      } catch (std::exception const &e) {
        printf("ERROR in param file \"%s\" on line %" PRId32 " : malformed base digit (invalid value) : %s\n",
               file_name, cur_line, line.words[i]);
        exit(EXIT_FAILURE);
      }

      if(digit < 0)
      {
        printf("ERROR in param file \"%s\" on line %" PRId32 " : negative values aren't supported for base digits : %s\n",
               file_name, cur_line, line.words[i]);
        exit(EXIT_FAILURE);
      }

      switch(line.words[i][0])
      {
      case 'M':
        values.aa_base_m[std::to_underlying(amino_acid)] = digit;
        break;
      case 'W':
        values.aa_base_w[std::to_underlying(amino_acid)] = digit;
        break;
      case 'H':
        values.aa_base_h[std::to_underlying(amino_acid)] = digit;
        break;
      }
    }
  }
  #endif
  else
  {
    printf("ERROR in param file \"%s\" on line %" PRId32 " : undefined key word \"%s\"\n", file_name, cur_line, line.words[0]);
    exit(EXIT_FAILURE);
  }
}

void ParamReader::stale_option_error(const std::string file_name,
                                     int32_t cur_line,
                                     const std::string option,
                                     const std::string comment) {
  std::cout << "ERROR in param file: " << file_name << ':' << cur_line << ":\n"
            << "\t" << option << " is no longer a valid option.\n";
  if (not comment.empty()) std::cout << "\t" << comment << ".\n";
  exit(EXIT_FAILURE);
}

} // namespace aevol
