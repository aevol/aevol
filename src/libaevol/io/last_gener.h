// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_LAST_GENER_H_
#define AEVOL_LAST_GENER_H_

#include <fstream>

#include "ae_types.h"
#include "AeTime.h"
#include "macros.h"

namespace aevol {

void write_last_gener_file(aevol::time_type last_gener);
auto get_last_gener_from_file() -> aevol::time_type;


inline void write_last_gener_file(aevol::time_type last_gener) {
  std::ofstream last_gener_file(LAST_GENER_FNAME);
  last_gener_file << last_gener << std::endl;
  last_gener_file.close();
}

inline auto get_last_gener_from_file() -> aevol::time_type {
  auto time = aevol::time_type{0};

  std::ifstream lg_file(LAST_GENER_FNAME);
  if (lg_file) {
    lg_file >> time;
  } else {
    std::cerr << "failed to open " << LAST_GENER_FNAME << std::endl;
    exit(1);
  }

  return time;
}

}  // namespace aevol

#endif  // AEVOL_LAST_GENER_H_
