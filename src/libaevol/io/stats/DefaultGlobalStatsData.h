// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_DEFAULTGLOBALSTATSDATA_H_
#define AEVOL_DEFAULTGLOBALSTATSDATA_H_

#include <tuple>
#include <concepts>

#include "AeTime.h"
#include "ae_types.h"

namespace aevol {

class DefaultGlobalStatsData {
 public:
  DefaultGlobalStatsData()                                          = delete;
  DefaultGlobalStatsData(const DefaultGlobalStatsData&)             = delete;
  DefaultGlobalStatsData(DefaultGlobalStatsData&&)                  = delete;
  DefaultGlobalStatsData& operator=(const DefaultGlobalStatsData&)  = delete;
  DefaultGlobalStatsData& operator=(DefaultGlobalStatsData&&)       = delete;
  virtual ~DefaultGlobalStatsData()                                 = delete;

  static auto header() { return "Generation,pop_size,indiv_idx"; }
  static auto collect_data(time_type generation, size_t pop_size, indiv_id_type indiv_idx);
};

inline auto DefaultGlobalStatsData::collect_data(time_type generation,
                                                 size_t pop_size,
                                                 indiv_id_type indiv_idx) {
  return std::make_tuple(generation, pop_size, indiv_idx);
}

}  // namespace aevol

#endif  // AEVOL_DEFAULTGLOBALSTATSDATA_H_
