// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_DIPLOIDMUTATIONSSTATSDATA_H_
#define AEVOL_DIPLOIDMUTATIONSSTATSDATA_H_

#include <tuple>

#include "Individual.h"
#include "MutationsStatsData.h"

namespace aevol {

class DiploidMutationsStatsData {
 public:
  DiploidMutationsStatsData()                                            = delete;
  DiploidMutationsStatsData(const DiploidMutationsStatsData&)            = delete;
  DiploidMutationsStatsData(DiploidMutationsStatsData&&)                 = delete;
  DiploidMutationsStatsData& operator=(const DiploidMutationsStatsData&) = delete;
  DiploidMutationsStatsData& operator=(DiploidMutationsStatsData&&)      = delete;
  virtual ~DiploidMutationsStatsData()                                   = delete;

  static auto header() { return MutationsStatsData::header(); }
  static auto collect_data(const Individual& indiv);
};

inline auto DiploidMutationsStatsData::collect_data(const Individual& indiv) {
  const auto& mutation_metrics_A = indiv.dna(Chrsm::A).mutation_metrics_;
  const auto& mutation_metrics_B = indiv.dna(Chrsm::B).mutation_metrics_;
  return std::make_tuple(mutation_metrics_A.nb_local_mutations + mutation_metrics_B.nb_local_mutations,
                         mutation_metrics_A.nb_switch + mutation_metrics_B.nb_switch,
                         mutation_metrics_A.nb_indels + mutation_metrics_B.nb_indels,
                         mutation_metrics_A.nb_rearrangements + mutation_metrics_B.nb_rearrangements,
                         mutation_metrics_A.nb_large_dupl + mutation_metrics_B.nb_large_dupl,
                         mutation_metrics_A.nb_large_del + mutation_metrics_B.nb_large_del,
                         mutation_metrics_A.nb_large_trans + mutation_metrics_B.nb_large_trans,
                         mutation_metrics_A.nb_large_inv + mutation_metrics_B.nb_large_inv);
}

}  // namespace aevol

#endif  // AEVOL_DIPLOIDMUTATIONSSTATSDATA_H_
