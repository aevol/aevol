// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_STATSDATACONCEPTS_H_
#define AEVOL_STATSDATACONCEPTS_H_

#include <concepts>
#include <ostream>
#include <string>

#include "AeTime.h"
#include "ae_types.h"

namespace aevol {

class Individual;

template <typename T>
concept StatsData = requires(T t, std::ostream& os) {
  { os << t.header() };
};

template <typename T>
concept GlobalStatsData =
    StatsData<T> &&
    requires(T t, std::ostream& os, time_type generation, size_t pop_size, indiv_id_type indiv_idx) {
      { os << t.collect_data(generation, pop_size, indiv_idx) };
    };

template <typename T>
concept SingleIndividualStatsData =
    StatsData<T> &&
    requires(T t, std::ostream& os, const Individual& indiv) {
      { os << t.collect_data(indiv) };
    };

template <typename T>
concept MultiIndividualStatsData =
    StatsData<T> &&
    requires(T t, const std::ranges::subrange<const Individual*>& indiv_range) {
      { collect_data<T>(indiv_range) } -> std::same_as<std::vector<std::vector<double>>>;
    };

}  // namespace aevol

#endif  // AEVOL_STATSDATACONCEPTS_H_
