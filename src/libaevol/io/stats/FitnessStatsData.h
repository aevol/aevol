// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_FITNESSSTATSDATA_H_
#define AEVOL_FITNESSSTATSDATA_H_

#include <ranges>
#include <tuple>

#include "Individual.h"

namespace aevol {

class FitnessStatsData {
 public:
  FitnessStatsData()                                   = delete;
  FitnessStatsData(const FitnessStatsData&)            = delete;
  FitnessStatsData(FitnessStatsData&&)                 = delete;
  FitnessStatsData& operator=(const FitnessStatsData&) = delete;
  FitnessStatsData& operator=(FitnessStatsData&&)      = delete;
  virtual ~FitnessStatsData()                          = delete;

  static auto header() { return "fitness,metabolic_error"; }
  static auto collect_data(const Individual& indiv);
};

inline auto FitnessStatsData::collect_data(const Individual& indiv) {
  return std::make_tuple(indiv.fitness(),
                         indiv.metabolic_error());
}

}  // namespace aevol

#endif  // AEVOL_FITNESSSTATSDATA_H_
