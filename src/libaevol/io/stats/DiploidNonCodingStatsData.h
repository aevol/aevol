// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_DIPLOIDNONCODINGSTATSDATA_H_
#define AEVOL_DIPLOIDNONCODINGSTATSDATA_H_

#include <tuple>

#include "Individual.h"

namespace aevol {

class DiploidNonCodingStatsData {
 public:
  DiploidNonCodingStatsData()                                            = delete;
  DiploidNonCodingStatsData(const DiploidNonCodingStatsData&)            = delete;
  DiploidNonCodingStatsData(DiploidNonCodingStatsData&&)                 = delete;
  DiploidNonCodingStatsData& operator=(const DiploidNonCodingStatsData&) = delete;
  DiploidNonCodingStatsData& operator=(DiploidNonCodingStatsData&&)      = delete;
  virtual ~DiploidNonCodingStatsData()                                   = default;

  static auto header();
  static auto collect_data(const Individual& indiv);
};

inline auto DiploidNonCodingStatsData::header() {
  return "non_coding_bp_small_chrsm,non_coding_bp_big_chrsm";
}

inline auto DiploidNonCodingStatsData::collect_data(const Individual& indiv) {
  auto chrsms_by_length =
      indiv.annotated_chromosome(Chrsm::A).length() < indiv.annotated_chromosome(Chrsm::B).length()
          ? std::make_pair(Chrsm::A, Chrsm::B)
          : std::make_pair(Chrsm::B, Chrsm::A);
  return std::make_tuple(indiv.annotated_chromosome(chrsms_by_length.first).compute_non_coding(),
                         indiv.annotated_chromosome(chrsms_by_length.second).compute_non_coding());
}

}  // namespace aevol

#endif  // AEVOL_DIPLOIDNONCODINGSTATSDATA_H_
