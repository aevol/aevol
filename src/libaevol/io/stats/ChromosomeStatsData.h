// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_CHROMOSOMESTATSDATA_H_
#define AEVOL_CHROMOSOMESTATSDATA_H_

#include <ranges>
#include <tuple>

#include "Individual.h"

namespace aevol {

class ChromosomeStatsData {
 public:
  ChromosomeStatsData()                                      = delete;
  ChromosomeStatsData(const ChromosomeStatsData&)            = delete;
  ChromosomeStatsData(ChromosomeStatsData&&)                 = delete;
  ChromosomeStatsData& operator=(const ChromosomeStatsData&) = delete;
  ChromosomeStatsData& operator=(ChromosomeStatsData&&)      = delete;
  virtual ~ChromosomeStatsData()                             = delete;

  static auto header();
  static auto collect_data(const Individual& indiv);
};

inline auto ChromosomeStatsData::header() {
  return "amount_of_dna,nb_coding_rnas,av_len_coding_rnas,nb_non_coding_rnas,"
         "nb_functional_genes,av_len_functional_genes,nb_non_functional_genes";
}

inline auto ChromosomeStatsData::collect_data(const Individual& indiv) {
  return std::tuple_cat(std::make_tuple(indiv.dna().length()),
                        indiv.annotated_chromosome().compute_rna_stats(),
                        indiv.annotated_chromosome().compute_gene_stats());
}

}  // namespace aevol

#endif  // AEVOL_CHROMOSOMESTATSDATA_H_
