// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_ABSTRACTSINGLEINDIVIDUALSTATSHANDLER_H_
#define AEVOL_ABSTRACTSINGLEINDIVIDUALSTATSHANDLER_H_

#include "AbstractStatsHandler.h"
#include "Individual.h"

namespace aevol {

class AbstractSingleIndividualStatsHandler: public AbstractStatsHandler {
 public:
  AbstractSingleIndividualStatsHandler()                                                        = default;
  AbstractSingleIndividualStatsHandler(const AbstractSingleIndividualStatsHandler&)             = delete;
  AbstractSingleIndividualStatsHandler(AbstractSingleIndividualStatsHandler&&)                  = delete;
  AbstractSingleIndividualStatsHandler& operator=(const AbstractSingleIndividualStatsHandler&)  = delete;
  AbstractSingleIndividualStatsHandler& operator=(AbstractSingleIndividualStatsHandler&&)       = delete;
  virtual ~AbstractSingleIndividualStatsHandler()                                               = default;

  virtual void write_data(time_type generation,
                          size_t pop_size,
                          indiv_id_type indiv_idx,
                          const Individual& indiv) = 0;
};

}  // namespace aevol

#endif  // AEVOL_ABSTRACTSINGLEINDIVIDUALSTATSHANDLER_H_
