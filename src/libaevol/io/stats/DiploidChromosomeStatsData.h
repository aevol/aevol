// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_DIPLOIDCHROMOSOMESTATSDATA_H_
#define AEVOL_DIPLOIDCHROMOSOMESTATSDATA_H_

#include <ranges>
#include <tuple>

#include "ChromosomeStatsData.h"
#include "Individual.h"

namespace aevol {

class DiploidChromosomeStatsData {
 public:
  DiploidChromosomeStatsData()                                             = delete;
  DiploidChromosomeStatsData(const DiploidChromosomeStatsData&)            = delete;
  DiploidChromosomeStatsData(DiploidChromosomeStatsData&&)                 = delete;
  DiploidChromosomeStatsData& operator=(const DiploidChromosomeStatsData&) = delete;
  DiploidChromosomeStatsData& operator=(DiploidChromosomeStatsData&&)      = delete;
  virtual ~DiploidChromosomeStatsData()                                    = delete;

  static auto header();
  static auto collect_data(const Individual& indiv);
};

inline auto DiploidChromosomeStatsData::header() {
  return "amount_of_dna_small_chrsm,"
         "nb_coding_rnas_small_chrsm,av_len_coding_rnas_small_chrsm,nb_non_coding_rnas_small_chrsm,"
         "nb_functional_genes_small_chrsm,av_len_functional_genes_small_chrsm,nb_non_functional_genes_small_chrsm,"
         "amount_of_dna_big_chrsm,"
         "nb_coding_rnas_big_chrsm,av_len_coding_rnas_big_chrsm,nb_non_coding_rnas_big_chrsm,"
         "nb_functional_genes_big_chrsm,av_len_functional_genes_big_chrsm,nb_non_functional_genes_big_chrsm";
}

inline auto DiploidChromosomeStatsData::collect_data(const Individual& indiv) {
  auto chrsms_by_length =
      indiv.annotated_chromosome(Chrsm::A).length() < indiv.annotated_chromosome(Chrsm::B).length()
          ? std::make_pair(Chrsm::A, Chrsm::B)
          : std::make_pair(Chrsm::B, Chrsm::A);
  return std::tuple_cat(std::make_tuple(indiv.annotated_chromosome(chrsms_by_length.first).length()),
                        indiv.annotated_chromosome(chrsms_by_length.first).compute_rna_stats(),
                        indiv.annotated_chromosome(chrsms_by_length.first).compute_gene_stats(),
                        std::make_tuple(indiv.annotated_chromosome(chrsms_by_length.second).length()),
                        indiv.annotated_chromosome(chrsms_by_length.second).compute_rna_stats(),
                        indiv.annotated_chromosome(chrsms_by_length.second).compute_gene_stats());
}

}  // namespace aevol

#endif  // AEVOL_CHROMOSOMESTATSDATA_H_
