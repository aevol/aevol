// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_STATSHANDLER_H_
#define AEVOL_STATSHANDLER_H_

#include <filesystem>
#include <utility>

#include "AeTime.h"
#include "StatsDataConcepts.h"

namespace aevol {

template <GlobalStatsData Head, SingleIndividualStatsData... Tail>
class SingleIndividualStatsHandler: public AbstractSingleIndividualStatsHandler {
 protected:
  SingleIndividualStatsHandler()                                               = default;
  SingleIndividualStatsHandler(const SingleIndividualStatsHandler&)            = delete;
  SingleIndividualStatsHandler(SingleIndividualStatsHandler&&)                 = delete;
  SingleIndividualStatsHandler& operator=(const SingleIndividualStatsHandler&) = delete;
  SingleIndividualStatsHandler& operator=(SingleIndividualStatsHandler&&)      = delete;

 public:
  static auto make_empty(std::filesystem::path path) -> std::unique_ptr<SingleIndividualStatsHandler>;
  static auto make_resume(std::filesystem::path path, time_type generation)
      -> std::unique_ptr<SingleIndividualStatsHandler>;

  virtual ~SingleIndividualStatsHandler() = default;

  void write_header() override;
  void write_data(time_type generation,
                  size_t pop_size,
                  indiv_id_type indiv_idx,
                  const Individual& indiv) override;
};

template <GlobalStatsData Head, SingleIndividualStatsData... Tail>
auto SingleIndividualStatsHandler<Head, Tail...>::make_empty(std::filesystem::path path)
    -> std::unique_ptr<SingleIndividualStatsHandler> {
  auto new_stats_handler =
      std::unique_ptr<SingleIndividualStatsHandler>(new SingleIndividualStatsHandler<Head, Tail...>());
  // Protected ctor -> can't use make_unique

  new_stats_handler->prepare_empty(path);

  return new_stats_handler;
}

template <GlobalStatsData Head, SingleIndividualStatsData... Tail>
auto SingleIndividualStatsHandler<Head, Tail...>::make_resume(std::filesystem::path path, time_type generation)
    -> std::unique_ptr<SingleIndividualStatsHandler> {
  auto new_stats_handler = std::unique_ptr<SingleIndividualStatsHandler>(new SingleIndividualStatsHandler());
  // Protected ctor -> can't use make_unique

  new_stats_handler->prepare_for_resume(path, generation);

  return new_stats_handler;
}

template <GlobalStatsData Head, SingleIndividualStatsData... Tail>
void SingleIndividualStatsHandler<Head, Tail...>::write_header() {
  statfile_ << Head::header();
  (..., (statfile_ << "," << Tail::header())); // Left fold; write comma separated headers for Tail...
  statfile_ << std::endl;
}

template <GlobalStatsData Head, SingleIndividualStatsData... Tail>
void SingleIndividualStatsHandler<Head, Tail...>::write_data(time_type generation,
                                                             size_t pop_size,
                                                             indiv_id_type indiv_idx,
                                                             const Individual& indiv) {
  statfile_ << Head::collect_data(generation, pop_size, indiv_idx);
  (..., (statfile_ << "," << Tail::collect_data(indiv))); // Left fold; write comma separated data for Tail...
  statfile_ << std::endl;
}

}  // namespace aevol

#endif  // AEVOL_STATSHANDLER_H_
