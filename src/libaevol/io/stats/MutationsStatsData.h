// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_MUTATIONSSTATSDATA_H_
#define AEVOL_MUTATIONSSTATSDATA_H_

#include <tuple>

#include "Individual.h"

namespace aevol {

class MutationsStatsData {
 public:
  MutationsStatsData()                                     = delete;
  MutationsStatsData(const MutationsStatsData&)            = delete;
  MutationsStatsData(MutationsStatsData&&)                 = delete;
  MutationsStatsData& operator=(const MutationsStatsData&) = delete;
  MutationsStatsData& operator=(MutationsStatsData&&)      = delete;
  virtual ~MutationsStatsData()                            = delete;

  static auto header() { return "nb_mut,nb_switch,nb_indels,nb_rear,nb_dupl,nb_del,nb_trans,nb_inv"; }
  static auto collect_data(const Individual& indiv);
};

inline auto MutationsStatsData::collect_data(const Individual& indiv) {
  const auto& mutation_metrics = indiv.dna().mutation_metrics_;
  return std::make_tuple(mutation_metrics.nb_local_mutations,
                         mutation_metrics.nb_switch,
                         mutation_metrics.nb_indels,
                         mutation_metrics.nb_rearrangements,
                         mutation_metrics.nb_large_dupl,
                         mutation_metrics.nb_large_del,
                         mutation_metrics.nb_large_trans,
                         mutation_metrics.nb_large_inv);
}

}  // namespace aevol

#endif  // AEVOL_MUTATIONSSTATSDATA_H_
