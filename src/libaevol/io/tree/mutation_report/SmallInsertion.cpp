// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "SmallInsertion.h"

#include <cstring>

#include "utility.h"

namespace aevol {

SmallInsertion::SmallInsertion(const SmallInsertion& other) : pos_(other.pos_), length_(other.length_) {
  seq_ = new char[length_ + 1];
  memcpy(seq_, other.seq_, length_ + 1);
}

SmallInsertion::SmallInsertion(int32_t pos, int32_t length, const char* seq) : pos_(pos), length_(length) {
  seq_ = new char[length_ + 1];
  memcpy(seq_, seq, length_);
  seq_[length_] = '\0';
}

SmallInsertion::~SmallInsertion() noexcept {
  delete[] seq_;
}

void SmallInsertion::save(gzFile tree_file) const {
  int8_t tmp_mut_type = std::to_underlying(MutationType::S_INS);
  gzwrite(tree_file, &tmp_mut_type, sizeof(tmp_mut_type));
  gzwrite(tree_file, &pos_, sizeof(pos_));
  gzwrite(tree_file, &length_, sizeof(length_));
  gzwrite(tree_file, seq_, length_ * sizeof(seq_[0]));
}

void SmallInsertion::load(gzFile tree_file) {
  gzread(tree_file, &pos_, sizeof(pos_));
  gzread(tree_file, &length_, sizeof(length_));
  seq_ = new char[length_ + 1];
  gzread(tree_file, seq_, length_ * sizeof(seq_[0]));
  seq_[length_] = '\0';
}

}  // namespace aevol
