// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_REPLACEMENT_HT_H_
#define AEVOL_REPLACEMENT_HT_H_

#include "HorizontalTransfer.h"

namespace aevol {

class ReplacementHT : public HorizontalTransfer {
 public:
  ReplacementHT()                                = default;
  ReplacementHT(const ReplacementHT&)            = default;
  ReplacementHT(ReplacementHT&&)                 = delete;
  ReplacementHT& operator=(const ReplacementHT&) = delete;
  ReplacementHT& operator=(ReplacementHT&&)      = delete;
  ~ReplacementHT() noexcept override;

  virtual auto Clone() const -> std::unique_ptr<MutationReport> override {
    return std::make_unique<ReplacementHT>(*this);
  };

  virtual void save(gzFile tree_file) const override;
  virtual void load(gzFile tree_file) override;

  virtual MutationType mut_type() const override { return MutationType::REPL_HT; };
  char* seq() const { return seq_; }
  int32_t length() const { return length_; }

 protected:
  int32_t length_;
  int32_t replaced_seq_length_;
  char* seq_        = nullptr;
  int32_t donor_id_ = -1;
};

}  // namespace aevol

#endif  //AEVOL_REPLACEMENT_HT_H_
