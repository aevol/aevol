// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "Duplication.h"

#include "utility.h"

namespace aevol {

void Duplication::save(gzFile tree_file) const {
  int8_t tmp_mut_type = std::to_underlying(MutationType::DUPL);
  gzwrite(tree_file, &tmp_mut_type, sizeof(tmp_mut_type));
  gzwrite(tree_file, &pos1_, sizeof(pos1_));
  gzwrite(tree_file, &pos2_, sizeof(pos2_));
  gzwrite(tree_file, &pos3_, sizeof(pos3_));
  gzwrite(tree_file, &length_, sizeof(length_));
  gzwrite(tree_file, &align_score_, sizeof(align_score_));
}

void Duplication::load(gzFile tree_file) {
  gzread(tree_file, &pos1_, sizeof(pos1_));
  gzread(tree_file, &pos2_, sizeof(pos2_));
  gzread(tree_file, &pos3_, sizeof(pos3_));
  gzread(tree_file, &length_, sizeof(length_));
  gzread(tree_file, &align_score_, sizeof(align_score_));
}

}  // namespace aevol
