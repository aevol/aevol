// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_SMALLINSERTION_H_
#define AEVOL_SMALLINSERTION_H_

#include <cstdint>

#include "LocalMutation.h"

namespace aevol {

class SmallInsertion : public LocalMutation {
 public:
  SmallInsertion()                                 = default;
  SmallInsertion(const SmallInsertion&);
  SmallInsertion(SmallInsertion&&)                 = delete;
  SmallInsertion& operator=(const SmallInsertion&) = delete;
  SmallInsertion& operator=(SmallInsertion&&)      = delete;
  ~SmallInsertion() noexcept override;

  SmallInsertion(int32_t pos, int32_t length, const char* seq);

  virtual auto Clone() const -> std::unique_ptr<MutationReport> override {
    return std::make_unique<SmallInsertion>(*this);
  };

  virtual void save(gzFile tree_file) const override;
  virtual void load(gzFile tree_file) override;

  virtual MutationType mut_type() const override { return MutationType::S_INS; };
  int32_t pos() const { return pos_; };
  int32_t pos1() const override { return pos_; }
  int32_t length() const override { return length_; }
  char* seq() const { return seq_; };

 protected:
  int32_t pos_;
  int32_t length_;
  char* seq_;
};

}  // namespace aevol

#endif  //AEVOL_SMALLINSERTION_H_
