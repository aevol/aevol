// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_INVERSION_H_
#define AEVOL_INVERSION_H_

#include "Rearrangement.h"

namespace aevol {

class Inversion : public Rearrangement {
 public:
  Inversion()                            = default;
  Inversion(const Inversion&)            = default;
  Inversion(Inversion&&)                 = delete;
  Inversion& operator=(const Inversion&) = delete;
  Inversion& operator=(Inversion&&)      = delete;
  ~Inversion() noexcept override         = default;

  Inversion(int32_t pos1, int32_t pos2, int32_t length, int16_t align_score = -1);

  virtual auto Clone() const -> std::unique_ptr<MutationReport> override { return std::make_unique<Inversion>(*this); };

  virtual void save(gzFile tree_file) const override;
  virtual void load(gzFile tree_file) override;

  virtual MutationType mut_type() const override { return MutationType::INV; };
  int32_t pos1() const override { return pos1_; }
  int32_t pos2() const override { return pos2_; }
  int16_t align_score() const override { return align_score_; }
  int32_t length() const override { return length_; }

 protected:
  int32_t pos1_, pos2_;
  int32_t length_;
  int16_t align_score_ = -1;
};

}  // namespace aevol

#endif  //AEVOL_INVERSION_H_
