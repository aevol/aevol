// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_POINTMUTATION_H_
#define AEVOL_POINTMUTATION_H_

#include "LocalMutation.h"

#include <cstdint>
#include <format>

#include "macros.h"

namespace aevol {

/**
 *
 */
class PointMutation : public LocalMutation {
 public:
  PointMutation()                                = default;
  PointMutation(const PointMutation&)            = default;
  PointMutation(PointMutation&&)                 = delete;
  PointMutation& operator=(const PointMutation&) = delete;
  PointMutation& operator=(PointMutation&&)      = delete;
  ~PointMutation() noexcept override             = default;

#ifdef BASE_2
  PointMutation(int32_t pos);
#elif BASE_4
  PointMutation(int32_t pos, char choice);
#endif

  virtual auto Clone() const -> std::unique_ptr<MutationReport> override {
    return std::make_unique<PointMutation>(*this);
  };

  virtual void save(gzFile tree_file) const override;
  virtual void load(gzFile tree_file) override;

  virtual MutationType mut_type() const override { return MutationType::SWITCH; };
  int32_t pos() const { return pos_; }
  int32_t pos1() const override { return pos_; }

#ifdef BASE_4
  char base() const { return base_; }
  auto sequence() const -> std::string { return std::format("'{}'", num_to_atcg(base_)); }
#endif

 protected:
  int32_t pos_;

#ifdef BASE_4
  char base_;
#endif
};

}  // namespace aevol

#endif  //AEVOL_POINTMUTATION_H_
