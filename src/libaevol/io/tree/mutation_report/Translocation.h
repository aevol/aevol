// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_TRANSLOCATION_H_
#define AEVOL_TRANSLOCATION_H_

#include "Rearrangement.h"

namespace aevol {

class Translocation : public Rearrangement {
 public:
  Translocation()                                = default;
  Translocation(const Translocation&)            = default;
  Translocation(Translocation&&)                 = delete;
  Translocation& operator=(const Translocation&) = delete;
  Translocation& operator=(Translocation&&)      = delete;
  ~Translocation() noexcept override             = default;

  Translocation(int32_t pos1,
                int32_t pos2,
                int32_t pos3,
                int32_t pos4,
                int32_t length,
                bool invert,
                int16_t align_score_1 = -1,
                int16_t align_score_2 = -1);

  virtual auto Clone() const -> std::unique_ptr<MutationReport> override {
    return std::make_unique<Translocation>(*this);
  };

  virtual void save(gzFile tree_file) const override;
  virtual void load(gzFile tree_file) override;

  virtual MutationType mut_type() const override { return MutationType::TRANS; };
  int32_t pos1() const override { return pos1_; }
  int32_t pos2() const override { return pos2_; }
  int32_t pos3() const override { return pos3_; }
  int32_t pos4() const override { return pos4_; }
  int8_t invert() const override { return invert_; }
  int16_t align_score() const override { return align_score_1_; }
  int16_t align_score_2() const override { return align_score_2_; }
  int32_t length() const override { return length_; }

 protected:
  int32_t pos1_, pos2_, pos3_, pos4_;
  int32_t length_;
  bool invert_;
  int16_t align_score_1_ = -1;
  int16_t align_score_2_ = -1;
};

}  // namespace aevol

#endif  //AEVOL_TRANSLOCATION_H_
