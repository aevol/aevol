// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_INSERTION_HT_H_
#define AEVOL_INSERTION_HT_H_

#include "HorizontalTransfer.h"

namespace aevol {

class InsertionHT : public HorizontalTransfer {
 public:
  InsertionHT()                              = default;
  InsertionHT(const InsertionHT&)            = default;
  InsertionHT(InsertionHT&&)                 = delete;
  InsertionHT& operator=(const InsertionHT&) = delete;
  InsertionHT& operator=(InsertionHT&&)      = delete;
  ~InsertionHT() noexcept override;

  virtual auto Clone() const -> std::unique_ptr<MutationReport> override {
    return std::make_unique<InsertionHT>(*this);
  };

  virtual void save(gzFile tree_file) const override;
  virtual void load(gzFile tree_file) override;

  virtual MutationType mut_type() const override { return MutationType::INS_HT; };
  int32_t length() const { return length_; }
  char* seq() const { return seq_; }
  int32_t donor_id() const { return donor_id_; }

 protected:
  int32_t length_;
  char* seq_        = nullptr;
  int32_t donor_id_ = -1;
};

}  // namespace aevol

#endif  //AEVOL_INSERTION_HT_H_
