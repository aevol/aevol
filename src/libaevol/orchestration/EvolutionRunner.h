// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_EVOLUTION_RUNNER_H_
#define AEVOL_EVOLUTION_RUNNER_H_

#include <memory>
#include <optional>

#include "ae_types.h"
#include "io/checkpointing/CheckpointHandler.h"
#include "io/stats/StatsOrchestrator.h"
#include "io/tree/PhylogeneticTreeHandler.h"
#include "io/ui/UserInterfaceOutput.h"
#include "phenotype/PhenotypicTarget.h"
#include "population/Grid.h"
#include "population/Population.h"

namespace aevol {

class EvolutionRunner {
 protected:
  EvolutionRunner();
  EvolutionRunner(const EvolutionRunner&)            = delete;
  EvolutionRunner(EvolutionRunner&&)                 = delete;
  EvolutionRunner& operator=(const EvolutionRunner&) = delete;
  EvolutionRunner& operator=(EvolutionRunner&&)      = delete;
 public:
  virtual ~EvolutionRunner()                         = default;

  static auto make_from_checkpoint(time_type time) -> std::unique_ptr<EvolutionRunner>;

  auto population_size() const { return population_.size(); }
  const auto& individual(indiv_id_type id) const { return population_[id]; }
  const auto& population() const { return population_; }
  auto& population() { return population_; }
  const auto& best_indiv() const { return population_.best_indiv(); }
  auto best_indiv_idx() const { return population_.best_indiv_idx(); }
  const auto& target() const { return *target_; }

  void setup_ui_output(const std::filesystem::path& outdir, int32_t frequency);

  void run_evolution(time_type t_end, double selection_pressure);

 protected:
  Grid grid_;
  Population population_;
  std::unique_ptr<PhenotypicTarget> target_;

  time_type checkpoint_frequency_ = 1000;
  std::unique_ptr<CheckpointHandler> checkpoint_handler_;
  std::unique_ptr<PhylogeneticTreeHandler> tree_handler_;
  std::optional<time_type> tree_output_frequency_ = std::nullopt;
  std::unique_ptr<UserInterfaceOutput> user_interface_output_;

  std::unique_ptr<StatsOrchestrator> stats_orchestrator_ = nullptr;

  static constexpr auto whole_reevaluation_required() { return false; };

  void load_checkpoint(time_type time);
  void write_checkpoint() const;

  void init_stats();
  void run_a_step(double w_max,
                  double selection_pressure,
                  std::vector<std::shared_ptr<Individual>>& next_generation_individuals,
                  std::vector<reproducers_info_t>& next_generation_reproducers,
                  std::vector<std::pair<indiv_id_type, std::unique_ptr<Individual::mutator_type>>>& mutant_list,
                  std::set<std::shared_ptr<Individual>>& non_mutants_to_reevaluate,
                  std::vector<std::pair<std::shared_ptr<Individual>, bool>>& indivs_to_reevaluate);

  auto handle_clone(indiv_id_type indiv_id, indiv_id_type parent_id) -> std::shared_ptr<Individual>;
  #ifndef SEX
  auto create_offspring(indiv_id_type indiv_id, reproducers_info_t reproducers_info) -> std::unique_ptr<Individual>;
  #else
  auto create_offspring(indiv_id_type indiv_id, reproducers_info_t reproducers_info, JumpingMT& prng)
      -> std::unique_ptr<Individual>;
  #endif

  void write_ui_output() const;
};

}  // namespace aevol

#endif  // AEVOL_EVOLUTION_RUNNER_H_
