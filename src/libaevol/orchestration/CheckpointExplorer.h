// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_CHECKPOINT_EXPLORER_H_
#define AEVOL_CHECKPOINT_EXPLORER_H_

#include <memory>

#include "ae_types.h"
#include "io/checkpointing/CheckpointHandler.h"
#include "phenotype/PhenotypicTarget.h"
#include "population/Grid.h"
#include "population/Population.h"

namespace aevol {

class CheckpointExplorer {
 protected:
  CheckpointExplorer()                                     = default;
  CheckpointExplorer(const CheckpointExplorer&)            = delete;
  CheckpointExplorer(CheckpointExplorer&&)                 = delete;
  CheckpointExplorer& operator=(const CheckpointExplorer&) = delete;
  CheckpointExplorer& operator=(CheckpointExplorer&&)      = delete;
 public:
  virtual ~CheckpointExplorer()                            = default;

  static auto make_from_checkpoint(time_type time) -> std::unique_ptr<CheckpointExplorer>;

  auto population_size() const { return population_.size(); }
  const auto& individual(indiv_id_type id) const { return population_[id]; }
  const auto& best_indiv() const { return population_.best_indiv(); }
  auto best_indiv_idx() const { return population_.best_indiv_idx(); }
  const auto& target() const { return *target_; }

  auto checkpoint_frequency() const { return checkpoint_frequency_; }
  auto tree_output_frequency() const { return tree_output_frequency_; }

 protected:
  Grid grid_;
  Population population_;
  std::unique_ptr<PhenotypicTarget> target_;

  time_type checkpoint_frequency_ = 1000;
  std::optional<time_type> tree_output_frequency_ = std::nullopt;

  void load_checkpoint(time_type time);
};

}  // namespace aevol

#endif  // AEVOL_CHECKPOINT_EXPLORER_H_
