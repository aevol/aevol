// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_INDIVIDUAL_ANALYSER_H_
#define AEVOL_INDIVIDUAL_ANALYSER_H_

#include <filesystem>
#include <memory>

#include "io/parameters/ParamValues.h"
#include "phenotype/PhenotypicTarget.h"

namespace aevol {

class IndividualAnalyser {
 protected:
  IndividualAnalyser()                                     = delete;
  IndividualAnalyser(const IndividualAnalyser&)            = delete;
  IndividualAnalyser(IndividualAnalyser&&)                 = delete;
  IndividualAnalyser(const std::filesystem::path& param_file_name);
  IndividualAnalyser& operator=(const IndividualAnalyser&) = delete;
  IndividualAnalyser& operator=(IndividualAnalyser&&)      = delete;
 public:
  virtual ~IndividualAnalyser()                            = default;

  static auto make_from_param_file(const std::filesystem::path& param_file_name) -> std::unique_ptr<IndividualAnalyser>;

  auto population_size() const { return param_values_->grid_width * param_values_->grid_height; }
  auto target() -> const PhenotypicTarget&;
  auto checkpoint_frequency() const { return param_values_->checkpoint_frequency; }

 protected:
  std::unique_ptr<ParamValues> param_values_ = nullptr;

  std::unique_ptr<PhenotypicTarget> target_ = nullptr;
};

}  // namespace aevol

#endif  // AEVOL_INDIVIDUAL_ANALYSER_H_
