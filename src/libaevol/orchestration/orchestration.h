// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_ORCHESTRATION_H_
#define AEVOL_ORCHESTRATION_H_

#include "ae_types.h"
#include "CheckpointExplorer.h"
#include "EvolutionRunner.h"
#include "ExperimentCreator.h"
#include "IndividualAnalyser.h"

namespace aevol {

auto make_experiment_creator_from_params(const ParamValues& params) -> std::unique_ptr<ExperimentCreator>;
auto make_evolution_runner_from_checkpoint(time_type time) -> std::unique_ptr<EvolutionRunner>;
auto make_checkpoint_explorer_from_checkpoint(time_type time) -> std::unique_ptr<CheckpointExplorer>;
auto make_individual_analyser_from_param_file(const std::filesystem::path& param_file_name)
    -> std::unique_ptr<IndividualAnalyser>;

inline auto make_experiment_creator_from_params(const ParamValues& params) -> std::unique_ptr<ExperimentCreator> {
  return ExperimentCreator::make_from_params(params);
}

inline auto make_evolution_runner_from_checkpoint(time_type time) -> std::unique_ptr<EvolutionRunner> {
  return EvolutionRunner::make_from_checkpoint(time);
}

inline auto make_checkpoint_explorer_from_checkpoint(time_type time) -> std::unique_ptr<CheckpointExplorer> {
  return CheckpointExplorer::make_from_checkpoint(time);
}

inline auto make_individual_analyser_from_param_file(const std::filesystem::path& param_file_name)
    -> std::unique_ptr<IndividualAnalyser> {
  return IndividualAnalyser::make_from_param_file(param_file_name);
}


}  // namespace aevol

#endif  // AEVOL_ORCHESTRATION_H_
