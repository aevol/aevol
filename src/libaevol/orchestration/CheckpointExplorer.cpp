// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "CheckpointExplorer.h"

namespace aevol {

auto CheckpointExplorer::make_from_checkpoint(time_type time) -> std::unique_ptr<CheckpointExplorer> {
  auto checkpoint_explorer = std::unique_ptr<CheckpointExplorer>(new CheckpointExplorer());
  checkpoint_explorer->load_checkpoint(time);
  return checkpoint_explorer;
}

void CheckpointExplorer::load_checkpoint(time_type time) {
  auto checkpoint_handler = std::make_unique<CheckpointHandler>("checkpoints");

  // Set aevol clock
  AeTime::set_time(time);

  // Read checkpoint
  auto [indivs_idxs_map,
        grid,
        target,
        exp_setup_read,
        checkpoint_freq,
        tree_output_freq, // ignored
        best_indiv_stats_output_freq, // ignored
        whole_pop_stats_output_freq] = checkpoint_handler->read_checkpoint(time);

  auto nb_indivs = grid.width() * grid.height();

  // Init phenotypic target
  target_ = std::move(target);

  // Init grid
  grid_ = std::move(grid);

  // Init experiment setup
  aevol::exp_setup = std::move(exp_setup_read);

  // Init checkpoint frequency
  checkpoint_frequency_ = checkpoint_freq;

  // Init tree output frequency
  if (tree_output_freq) {
    tree_output_frequency_ = tree_output_freq;
  }

  // Evaluate the individuals
  for (auto& [indiv, idxs] : indivs_idxs_map) {
    indiv->evaluate(exp_setup->w_max(), exp_setup->selection_pressure(), *target_);
  }

  population_.reset_from_idxs_map(nb_indivs, indivs_idxs_map);
  population_.update_best();
}

}  // namespace aevol
