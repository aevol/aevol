// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef AEVOL_EXPERIMENT_CREATOR_H_
#define AEVOL_EXPERIMENT_CREATOR_H_

#include <memory>
#include <optional>

#include "ae_types.h"
#include "io/checkpointing/CheckpointHandler.h"
#include "io/parameters/ParamValues.h"
#include "io/stats/StatsOrchestrator.h"
#include "phenotype/PhenotypicTarget.h"
#include "population/Grid.h"
#include "population/Individual.h"
#include "population/Population.h"

namespace aevol {

class ExperimentCreator {
 protected:
  ExperimentCreator();
  ExperimentCreator(const ExperimentCreator&)            = delete;
  ExperimentCreator(ExperimentCreator&&)                 = delete;
  ExperimentCreator& operator=(const ExperimentCreator&) = delete;
  ExperimentCreator& operator=(ExperimentCreator&&)      = delete;
 public:
  virtual ~ExperimentCreator()                           = default;

  static auto make_from_params(const ParamValues& params) -> std::unique_ptr<ExperimentCreator>;

  const auto& population() const { return population_; }
  const auto& target() const { return *target_; }

  void create_random_clonal_population(JumpingMT& prng, dna_size_type chrsm_init_len);
  void create_clonal_population_from_sequences(const std::vector<std::string>& sequences);

  void write_checkpoint() const;

 protected:
  Grid grid_;
  Population population_;
  std::unique_ptr<PhenotypicTarget> target_;

  time_type checkpoint_frequency_ = 1000;
  std::unique_ptr<CheckpointHandler> checkpoint_handler_;
  std::optional<time_type> tree_output_frequency_ = std::nullopt;
  std::unique_ptr<StatsOrchestrator> stats_orchestrator_ = nullptr;

  void fill_population_with_clones(std::unique_ptr<Individual> indiv);

  void load_params(const ParamValues& params);
};

}  // namespace aevol

#endif  // AEVOL_EXPERIMENT_CREATOR_H_
