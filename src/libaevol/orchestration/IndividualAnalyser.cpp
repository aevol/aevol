// ****************************************************************************
//
//          Aevol - An in silico experimental evolution platform
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package or <www.aevol.fr>
// Web: http://www.aevol.fr/
// E-mail: See <http://www.aevol.fr/contact/>
// Original Authors : Guillaume Beslon, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "IndividualAnalyser.h"

#include "ExpSetup.h"
#include "io/parameters/ParamReader.h"

namespace aevol {

IndividualAnalyser::IndividualAnalyser(const std::filesystem::path& param_file_name) {
  param_values_ = ParamReader::read_file(param_file_name.c_str());
}

auto IndividualAnalyser::make_from_param_file(const std::filesystem::path& param_file_name)
    -> std::unique_ptr<IndividualAnalyser> {
  auto individual_analysis_driver = std::unique_ptr<IndividualAnalyser>(new IndividualAnalyser(param_file_name));

  // Initialize the experimental setup now, the rest will be initialized lazily
  exp_setup->load_from_params(*individual_analysis_driver->param_values_);

  return individual_analysis_driver;
}

auto IndividualAnalyser::target() -> const PhenotypicTarget& {
  if (not target_) {
    target_ = PhenotypicTarget::make_from_gaussians(param_values_->std_env_gaussians,
                                                    param_values_->fuzzy_flavor,
                                                    param_values_->env_sampling);
  }
  return *target_;
}

}  // namespace aevol
